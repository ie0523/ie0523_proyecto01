`include "Mux2a1.v"
module MuxL1 (
    input [7:0] data_in_mux_L1_0,
    input [7:0] data_in_mux_L1_1,
    input [7:0] data_in_mux_L1_2,
    input [7:0] data_in_mux_L1_3,
    input clk_2f,
    input clk_f,
    input valid1,
    input selectorautomatico,
    input reset,
    output  [7:0] data_out_mux_L1_0,
    input valid0,
    output valid_out,
    output  [7:0] data_out_mux_L1_1
);

Mux2a1 mux1(
             .clk_f (clk_2f),
			.data_inm0 (data_in_mux_L1_0),
			.data_inm1 (data_in_mux_L1_1),
			.data_outm (data_out_mux_L1_0),
			.valid0  (valid0),
			.valid1  (valid1),
      .selectorautomatico  (selectorautomatico),
      .reset  (reset),
			.valid_out  (valid_out)
);

Mux2a1 mux2(
            .clk_f (clk_2f),
			.data_inm0 (data_in_mux_L1_2),
			.data_inm1 (data_in_mux_L1_3),
			.data_outm (data_out_mux_L1_1),
			.valid0  (valid0),
			.valid1  (valid0),
      .selectorautomatico  (selectorautomatico),
      .reset  (reset),
			.valid_out  (valid_out)
);

endmodule
