`timescale 	1ns				/ 100ps
`include "probadormuxsint.v"
`include "synthmuxL2.v"
`include "synthmuxL1.v"
`include "cmos_cells.v"
module testbench;
wire clk_f;
wire clk_2f;
wire [7:0] data_in_mux_L1_0;
wire [7:0] data_in_mux_L1_1;
wire [7:0] data_in_mux_L1_2;
wire [7:0] data_in_mux_L1_3;
wire [7:0] data_out_mux_L1_0;
wire [7:0] data_out_mux_L1_1;
wire [7:0] data_in_mux_L2_0;
wire [7:0] data_in_mux_L2_1;
wire clk_4f;
wire [7:0] data_out_mux_L2_0;
wire valid0;
wire valid1;
wire valid2;
wire valid3;
wire valid_out00;
wire valid_out11;
wire valid_out;
wire reset;

    MuxL1 mux12(     .clk_2f  (clk_2f),
                    .data_in_mux_L1_0 (data_in_mux_L1_0),
                    .data_in_mux_L1_1 (data_in_mux_L1_1),
                    .data_in_mux_L1_2 (data_in_mux_L1_2),
                    .data_in_mux_L1_3 (data_in_mux_L1_3),
                    .selectorautomatico  (clk_f),
                    .valid0    (valid0),
                    .reset    (reset),
                    .valid1    (valid1),
                    .valid_out  (valid_out),
                    .data_out_mux_L1_0    (data_out_mux_L1_0),
                    .data_out_mux_L1_1    (data_out_mux_L1_1)
);

    MuxL2 mux22(     .clk_4f  (clk_4f),
                    .data_in_mux_L2_0 (data_out_mux_L1_0),
                    .data_in_mux_L2_1 (data_out_mux_L1_1),
                    .valid0    (valid0),
                    .valid1    (valid1),
                    .reset    (reset),
                    .selectorautomatico  (clk_2f),
                    .valid_out   (valid_out),
                    .data_out_mux_L2_0    (data_out_mux_L2_0)
);


    probador probador_(     .clk_f  (clk_f),
                    .clk_2f  (clk_2f),
                    .data_in_mux_L1_0 (data_in_mux_L1_0),
                    .data_in_mux_L1_1 (data_in_mux_L1_1),
                    .data_in_mux_L1_2 (data_in_mux_L1_2),
                    .data_in_mux_L1_3 (data_in_mux_L1_3),
                    .data_out_mux_L1_0    (data_out_mux_L1_0),
                    .data_out_mux_L1_1    (data_out_mux_L1_1),
                    .clk_4f  (clk_4f),
                    .data_in_mux_L2_0 (data_in_mux_L2_0),
                    .data_in_mux_L2_1 (data_in_mux_L2_1),
                    .valid0    (valid0),
                    .valid1    (valid1),
                    .reset    (reset),
                    .valid_out   (valid_out),
                    .data_out_mux_L2_0    (data_out_mux_L2_0)
);
endmodule
