module demux_conductual (
    input clk,
    input [7:0] data_in,
    input validin,
    input selector,
    output reg valid_out0,
    output reg valid_out1,
    output reg [7:0] data_out0,
    output reg [7:0] data_out1
);
reg [7:0] aux0=0;
reg [7:0] aux1=0;

  always @ ( posedge clk ) begin
      aux1 <= data_out1;
      aux0 <= data_out0;
      end


    always @(*) begin
    data_out0=aux0;
    data_out1=aux1;
    valid_out0=0;
    valid_out1=0; 

    if (validin == 0) begin
      valid_out0=0;
      valid_out1=0;
    end

    else begin
      if (selector==0) begin
        data_out0 = data_in;
        valid_out0=validin;
      end
      if (selector==1) begin
        data_out1=data_in;
        valid_out1=validin;
      end

    end

  end


endmodule
