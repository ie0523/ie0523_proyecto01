`include "demux_1a2.v"
module demux_L1 (
input clk_f,
input [7:0] data_in_demux_L1_0,
input [7:0] data_in_demux_L1_1,
input validin,
input selector,
input selector2,
output valid_out0,
output  valid_out1,
output  [7:0] data_out_demux_L1_0,
output  [7:0] data_out_demux_L1_1,
output  [7:0] data_out_demux_L1_2,
output  [7:0] data_out_demux_L1_3
);

demux_conductual demux1( .clk (clk_f),
                .data_in    (data_in_demux_L1_0),
                .validin    (validin),
                .selector   (selector),
                .valid_out0 (valid_out0),
                .valid_out1 (valid_out1),
                .data_out0  (data_out_demux_L1_0),
                .data_out1  (data_out_demux_L1_1)
);
demux_conductual demux2(    .clk (clk_f),
                .data_in    (data_in_demux_L1_1),
                .validin    (validin),
                .selector   (selector2),
                .valid_out0 (valid_out0),
                .valid_out1 (valid_out1),
                .data_out0  (data_out_demux_L1_2),
                .data_out1  (data_out_demux_L1_3)
);
endmodule
