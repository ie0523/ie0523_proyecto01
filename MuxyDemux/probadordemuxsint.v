module probador (
output reg clk_f,
output reg [7:0] data_in_demux_L1_0,
output reg [7:0] data_in_demux_L1_1,
input valid_out0,
input valid_out1,
input [7:0] data_out_demux_L1_0,
input [7:0] data_out_demux_L1_1,
input [7:0] data_out_demux_L1_2,
input [7:0] data_out_demux_L1_3,
output reg clk_2f,
output reg [7:0] data_in_demux_L2_0,
output reg validin,
input [7:0] data_out_demux_L2_0,
input [7:0] data_out_demux_L2_1,
output reg clk_4f,
input [7:0] data_out_mux_L2_0
);

initial begin
   $dumpfile("Demux.vcd");
   $dumpvars;
    validin<=1;
    data_in_demux_L2_0<=0;
    @(posedge clk_4f);
    data_in_demux_L2_0<=8'hFF;
    @(posedge clk_4f);
    data_in_demux_L2_0<=8'hDD;
    @(posedge clk_4f);
    data_in_demux_L2_0<=8'hEE;
    @(posedge clk_4f);
    data_in_demux_L2_0<=8'hCC;
     @(posedge clk_4f);
    data_in_demux_L2_0<=8'hBB;
     @(posedge clk_4f);
    data_in_demux_L2_0<=8'h99;
     @(posedge clk_4f);
    data_in_demux_L2_0<=8'hAA;
    @(posedge clk_4f);
    data_in_demux_L2_0<=8'hFF;
    @(posedge clk_4f);
    data_in_demux_L2_0<=8'hDD;
    @(posedge clk_4f);
    data_in_demux_L2_0<=8'hEE;
    @(posedge clk_4f);
    data_in_demux_L2_0<=8'hCC;
     @(posedge clk_4f);
    data_in_demux_L2_0<=8'hBB;
     @(posedge clk_4f);
    data_in_demux_L2_0<=8'h99;
     @(posedge clk_4f);
    data_in_demux_L2_0<=8'hAA;

     $finish;
end

initial clk_f <= 0;
always #40 clk_f <= ~clk_f;


initial clk_2f <= 1;
always #20 clk_2f <= ~clk_2f;

initial clk_4f <= 1;
always #10 clk_4f <= ~clk_4f;

endmodule
