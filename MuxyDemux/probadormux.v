module probador (
output reg clk_f,
output reg clk_2f,
output reg [7:0] data_in_mux_L1_0,
output reg [7:0] data_in_mux_L1_1,
output reg [7:0] data_in_mux_L1_2,
output reg [7:0] data_in_mux_L1_3,
output reg reset,
input [7:0] data_out_mux_L1_0,
input [7:0] data_out_mux_L1_1,
output reg [7:0] data_in_mux_L2_0,
output reg [7:0] data_in_mux_L2_1,
output reg clk_4f,
input [7:0] data_out_mux_L2_0,
output reg valid0,
output reg valid1,
input valid_out
);

initial begin
   $dumpfile("Mux.vcd");
   $dumpvars;
    data_in_mux_L1_0<=0;
    data_in_mux_L1_1<=0;
    data_in_mux_L1_2<=0;
    data_in_mux_L1_3<=0;
    valid0<=1;
    valid1<=1;
    reset<=0;

    @(posedge clk_f);
    reset<=1;

    @(posedge clk_f);
    data_in_mux_L1_0<=8'hFF;
    data_in_mux_L1_1<=8'hEE;
    data_in_mux_L1_2<=8'hDD;
    data_in_mux_L1_3<=8'hCC;

    @(posedge clk_f);
    data_in_mux_L1_0<=8'hBB;
    data_in_mux_L1_1<=8'hAA;
    data_in_mux_L1_2<=8'h99;
    data_in_mux_L1_3<=8'h88;

    @(posedge clk_f);
    data_in_mux_L1_2<=8'h77;
     $finish;
end

initial clk_f <= 0;
always #40 clk_f <= ~clk_f;


initial clk_2f <= 1;
always #20 clk_2f <= ~clk_2f;

initial clk_4f <= 1;
always #10 clk_4f <= ~clk_4f;

endmodule
