`timescale 	1ns				/ 100ps
`include "probadordemuxsint.v"
`include "synthdemuxL1.v"
`include "synthdemuxL2.v"
`include "cmos_cells.v"
module testbench;
wire clk_f;
wire [7:0] data_in_demux_L1_0;
wire [7:0] data_in_demux_L1_1;
wire [7:0] data_out_demux_L1_0;
wire [7:0] data_out_demux_L1_1;
wire [7:0] data_out_demux_L1_2;
wire [7:0] data_out_demux_L1_3;
wire clk_2f;
wire [7:0] data_in_demux_L2_0;
wire validin;
wire valid_out0;
wire valid_out1;
wire [7:0] data_out_demux_L2_0;
wire [7:0] data_out_demux_L2_1;
wire clk_4f;




   demux_L1 demux1(     .clk_f  (clk_2f),
                    .data_in_demux_L1_0 (data_out_demux_L2_0),
                    .data_in_demux_L1_1 (data_out_demux_L2_1),
                    .validin    (validin),
                    .selector   (~clk_f),
                    .selector2 (~(clk_f^clk_2f)),
                    .valid_out0     (valid_out0),
                    .valid_out1     (valid_out1),
                    .data_out_demux_L1_0    (data_out_demux_L1_0),
                    .data_out_demux_L1_1    (data_out_demux_L1_1),
                    .data_out_demux_L1_2    (data_out_demux_L1_2),
                    .data_out_demux_L1_3    (data_out_demux_L1_3)
);
   demux_L2 demux2(     .clk_f  (clk_4f),
                    .data_in_demux_L2_0 (data_in_demux_L2_0),
                    .validin    (validin),
                    .selector   (~clk_2f),
                    .valid_out0     (valid_out0),
                    .valid_out1     (valid_out1),
                    .data_out_demux_L2_0    (data_out_demux_L2_0),
                    .data_out_demux_L2_1    (data_out_demux_L2_1)
);

    probador probador_(     .clk_f  (clk_f),
                    .data_in_demux_L1_0 (data_in_demux_L1_0),
                    .data_in_demux_L1_1 (data_in_demux_L1_1),
                    .data_out_demux_L1_0    (data_out_demux_L1_0),
                    .data_out_demux_L1_1    (data_out_demux_L1_1),
                    .data_out_demux_L1_2    (data_out_demux_L1_2),
                    .data_out_demux_L1_3    (data_out_demux_L1_3),
                    .clk_2f  (clk_2f),
                    .data_in_demux_L2_0 (data_in_demux_L2_0),
                    .validin    (validin),
                    .valid_out0     (valid_out0),
                    .valid_out1     (valid_out1),
                    .data_out_demux_L2_0    (data_out_demux_L2_0),
                    .data_out_demux_L2_1    (data_out_demux_L2_1),
                    .clk_4f  (clk_4f)

);
endmodule
