
module MuxL2 (
    input [7:0] data_in_mux_L2_0,
    input [7:0] data_in_mux_L2_1,
    input clk_4f,
    output valid_out,
    input valid0,
    input valid1,
    input selectorautomatico,
    input reset,
    output  [7:0] data_out_mux_L2_0
);

Mux2a1 mux3(
         .clk_f (clk_4f),
		 .data_inm0 (data_in_mux_L2_0),
		 .data_inm1 (data_in_mux_L2_1),
		 .data_outm (data_out_mux_L2_0),
		 .valid0  (valid0),
		 .valid1  (valid1),
         .selectorautomatico  (selectorautomatico),
         .reset  (reset),
		 .valid_out  (valid_out)
);

endmodule
