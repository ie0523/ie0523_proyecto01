module Recirculacionmod (
    input clk_f,
    input  selectorRe ,
    input [7:0] data_in0,
    input [7:0] data_in1,
    input [7:0] data_in2,
    input [7:0] data_in3,
    input valid0,
    input valid1,
    input valid2,
    input valid3,
    output reg valid_out0re,
    output reg valid_out1re,
    output reg valid_out2re,
    output reg valid_out3re,
    output reg valid_out4re,
    output reg valid_out5re,
    output reg valid_out6re,
    output reg valid_out7re,
    output reg [7:0] data_out0,
    output reg [7:0] data_out1,
    output reg [7:0] data_out2,
    output reg [7:0] data_out3,
    output reg [7:0] data_out4,
    output reg [7:0] data_out5,
    output reg [7:0] data_out6,
    output reg [7:0] data_out7
);

reg [7:0] out0;
reg [7:0] out1;
reg [7:0] out2;
reg [7:0] out3;
reg [7:0] out4;
reg [7:0] out5;
reg [7:0] out6;
reg [7:0] out7;

reg [7:0] out0v;
reg [7:0] out1v;
reg [7:0] out2v;
reg [7:0] out3v;
reg [7:0] out4v;
reg [7:0] out5v;
reg [7:0] out6v;
reg [7:0] out7v;

always @(posedge clk_f) begin
   data_out0<=out0;
   data_out1<=out1;
   data_out2<=out2;
   data_out3<=out3;
   data_out4<=out4;
   data_out5<=out5;
   data_out6<=out6;
   data_out7<=out7;
   valid_out0re<=out0v;
   valid_out1re<=out1v;
   valid_out2re <=out2v;
   valid_out3re <=out3v;
   valid_out4re <=out4v;
   valid_out5re <=out5v;
   valid_out6re <=out6v;
   valid_out7re <=out7v;
end

//0 No hay IDLE

always @(*) begin
    if (selectorRe == 1) begin
        out0=0;
        out1=0;
        out2=0;
        out3=0;

        out0v=0;
        out1v=0;
        out2v=0;
        out3v=0;

        out4=data_in0;
        out5=data_in1;
        out6=data_in2;
        out7=data_in3;

        out4v=valid0;
        out5v=valid1;
        out6v=valid2;
        out7v=valid3;


    end
    else  begin
       out0=data_in0;
       out1=data_in1;
       out2=data_in2;
       out3=data_in3;

       out4=0;
       out5=0;
       out6=0;
       out7=0;

        out4v=0;
        out5v=0;
        out6v=0;
        out7v=0;
        
        out0v=valid0;
        out1v=valid1;
        out2v=valid2;
        out3v=valid3;

       
    end

end

endmodule
