
module demux2 (
input clk_f,
input [7:0] data_in_demux_L2_0,
input validin,
input selector,
input reset,
output valid_out0,
output  valid_out1,
output  [7:0] data_out_demux_L2_0,
output  [7:0] data_out_demux_L2_1
);

demux_conductual demux3( .clk (clk_f),
                .data_in    (data_in_demux_L2_0),
                .validin    (validin),
                .selector   (selector),
                                    .reset (reset),
                .valid_out0 (valid_out0),
                .valid_out1 (valid_out1),
                .data_out0  (data_out_demux_L2_0),
                .data_out1  (data_out_demux_L2_1)
);
endmodule
