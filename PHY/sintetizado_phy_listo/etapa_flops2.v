module etapa_flops_2 (
    input clk,
    input [7:0] data0_flop2,
    input [7:0] data1_flop2,
    input valid0_flop2,
    input valid1_flop2,
    output reg validout0_flop2,
    output reg validout1_flop2,
    input reset,
    output reg [7:0] out0_flop2,
    output reg [7:0] out1_flop2
);

always @(posedge clk)begin
    if (reset==0) begin
    out0_flop2=0;
    out1_flop2=0;
    validout0_flop2=0;
    validout1_flop2=0;
    end else begin
    out0_flop2=data0_flop2;
    out1_flop2=data1_flop2;
    validout0_flop2=valid0_flop2;
    validout1_flop2=valid1_flop2;
    end
end

endmodule
