module paralelo_serial (
    input		 clk_4f,
	input		 clk_32f,
	input [0:7] 	data_in_paralelo_8bit,
	input		 valid,
	input 		 reset,
	output reg	 data_out_serial_1bit
);

reg [0:2] contador;
reg [0:7] aux;



always @(*) begin
    if (valid==0 && reset==1) begin
     aux=8'hBC;
    end

    else begin
        aux= data_in_paralelo_8bit;
    end
end
always @(posedge clk_32f)begin 
    if (reset==0) begin 
        contador<=0;
        data_out_serial_1bit<=0; 
    end 
    else begin 
    data_out_serial_1bit<= aux[contador];
    contador <=contador+1;
    end
end


endmodule