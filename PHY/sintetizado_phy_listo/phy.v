`include "phy_rx.v"
`include "phy_tx.v"

module tx_rx(
    input clk_f, 
    input clk_2f, 
    input clk_4f, 
    input clk_32f,
    input reset,
    input [7:0] data_in0_phy, 
    input [7:0] data_in1_phy,
    input [7:0] data_in2_phy,
    input [7:0] data_in3_phy,
    input valid0_phy,
    input valid1_phy,
    input valid2_phy,
    input valid3_phy,
    input selectorRe,
    output [0:7] data_out_0_phy,
    output [0:7] data_out_1_phy,
    output [0:7] data_out_2_phy,
    output  valid_out0_phy,
    output  valid_out1_phy,
    output  valid_out2_phy,
    output  valid_out3_phy,
    output [0:7] data_out_3_phy
);

wire data_out_tx;
wire data_out_ps_idle;
phy_tx phy1( .clk_f         (clk_f), 
             .clk_2f        (clk_2f), 
             .clk_4f        (clk_4f), 
             .clk_32f       (clk_32f),
             .data_out_tx   (data_out_tx),
             .data_in0_tx   (data_in0_phy), 
             .data_in1_tx   (data_in1_phy),
             .data_in2_tx   (data_in2_phy),
             .data_in3_tx   (data_in3_phy),
             .reset         (reset),
             .entrada_serial_paralelo    (data_out_ps_idle),
             .valid0_tx                  (valid0_phy),
             .valid1_tx                  (valid1_phy),
             .valid2_tx                  (valid2_phy),
             .valid3_tx                  (valid3_phy)
);

phy_rx phy2(  .clk_f         (clk_f), 
             .clk_2f        (clk_2f), 
             .clk_4f        (clk_4f), 
             .clk_32f       (clk_32f),
             .out_ps_tx      (data_out_tx),
             .data_out_0_rx   (data_out_0_phy),
             .data_out_1_rx   (data_out_1_phy), 
             .data_out_2_rx   (data_out_2_phy),
             .data_out_3_rx   (data_out_3_phy),
             .valid_out0_rx   (valid_out0_phy),
             .valid_out1_rx   (valid_out1_phy),
             .reset           (reset),
             .data_out_ps_idle (data_out_ps_idle),
             .valid_out2_rx   (valid_out2_phy),
             .valid_out3_rx   (valid_out3_phy)
);

endmodule
