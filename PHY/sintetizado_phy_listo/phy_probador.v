module phy_probador(
    output reg clk_f,
    output reg clk_2f,
    output reg clk_4f,
    output reg clk_32f,
    output reg reset,
    output reg  [7:0] data_in0_phy,
    output reg  [7:0] data_in1_phy,
    output reg  [7:0] data_in2_phy,
    output reg  [7:0] data_in3_phy,
    output reg valid1_phy,
    output reg valid2_phy,
    output reg valid3_phy,
    input  [7:0] data_out_0_phy,
    input  [7:0] data_out_1_phy,
    input  [7:0] data_out_2_phy,

    input valid_out0_phy,
    input valid_out1_phy,
    input valid_out2_phy,
    input  valid_out3_phy,
    input  [7:0] data_out_3_phy,
    output reg  valid0_phy
);

initial begin
   $dumpfile("phy.vcd");
   $dumpvars;
    data_in0_phy<=0;
    data_in1_phy<=0;
    data_in2_phy<=0;
    data_in3_phy<=0;
    valid0_phy<=1;
    valid1_phy<=1;
    valid2_phy<=1;
    valid3_phy<=1;
    reset<=0;
    @(posedge clk_f);
    reset<=0;
    @(posedge clk_f);
    reset<=1;

    @(posedge clk_f);
    data_in0_phy<=8'hFF;
    data_in1_phy<=8'hEE;
    data_in2_phy<=8'hDD;
    data_in3_phy<=8'hCC;

    @(posedge clk_f);
    data_in0_phy<=8'hBB;
    data_in1_phy<=8'hAA;
    data_in2_phy<=8'h99;
    data_in3_phy<=8'h88;



    @(posedge clk_f);

    data_in0_phy<=8'hFF;
    data_in1_phy<=8'hEE;
    data_in2_phy<=8'hDD;
    data_in3_phy<=8'hCC;

    @(posedge clk_f);

    data_in0_phy<=8'hBB;
    data_in1_phy<=8'hAA;
    data_in2_phy<=8'h99;
    data_in3_phy<=8'h88;


    @(posedge clk_f);

    data_in0_phy<=8'hFF;
    data_in1_phy<=8'hEE;
    data_in2_phy<=8'hDD;
    data_in3_phy<=8'hCC;

    @(posedge clk_f);

    data_in0_phy<=8'hBB;
    data_in1_phy<=8'hAA;
    data_in2_phy<=8'h99;
    data_in3_phy<=8'h88;
    @(posedge clk_f);

    data_in0_phy<=8'hFF;
    data_in1_phy<=8'hEE;
    data_in2_phy<=8'hDD;
    data_in3_phy<=8'hCC;

    @(posedge clk_f);

    data_in0_phy<=8'hBB;
    data_in1_phy<=8'hAA;
    data_in2_phy<=8'h99;
    data_in3_phy<=8'h88;

    @(posedge clk_f);

    data_in0_phy<=8'hFF;
    data_in1_phy<=8'hEE;
    data_in2_phy<=8'hDD;
    data_in3_phy<=8'hCC;

    @(posedge clk_f);

    data_in0_phy<=8'hBB;
    data_in1_phy<=8'hAA;
    data_in2_phy<=8'h99;
    data_in3_phy<=8'h88;

    @(posedge clk_f);

    data_in0_phy<=8'hFF;
    data_in1_phy<=8'hEE;
    data_in2_phy<=8'hDD;
    data_in3_phy<=8'hCC;

    @(posedge clk_f);

    data_in0_phy<=8'hBB;
    data_in1_phy<=8'hAA;
    data_in2_phy<=8'h99;
    data_in3_phy<=8'h88;

    @(posedge clk_f);

    data_in0_phy<=8'hFF;
    data_in1_phy<=8'hEE;
    data_in2_phy<=8'hDD;
    data_in3_phy<=8'hCC;

    @(posedge clk_f);

    data_in0_phy<=8'hBB;
    data_in1_phy<=8'hAA;
    data_in2_phy<=8'h99;
    data_in3_phy<=8'h88;




     $finish;
end

initial clk_f <= 0;
always #128 clk_f <= ~clk_f;

initial clk_2f <= 1;
always #64 clk_2f <= ~clk_2f;

initial clk_4f <= 1;
always #32 clk_4f <= ~clk_4f;

initial clk_32f <= 1;
always #4 clk_32f <= ~clk_32f;






endmodule
