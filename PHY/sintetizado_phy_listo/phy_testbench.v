`timescale 	1ns	/ 100ps
`include "tx_rx.v"
`include "phy_probador.v"
`include "cmos_cells.v"
`include "tx_rx_synth.v"
module BancoPruebas;
    wire clk_f; 
    wire clk_2f; 
    wire clk_4f; 
    wire clk_32f;
	wire [7:0] data_in0_phy;
    wire [7:0] data_in1_phy;
    wire [7:0] data_in2_phy;
    wire [7:0] data_in3_phy;
    wire valid0_phy;
    wire valid1_phy;
    wire valid2_phy;
    wire valid3_phy;
    wire [0:7] data_out_0_phy;
    wire [0:7] data_out_1_phy;
    wire [0:7] data_out_2_phy;
    wire [0:7] data_out_3_phy;
	wire [0:7] data_out_0_phyest;
    wire [0:7] data_out_1_phyest;
    wire [0:7] data_out_2_phyest;
    wire [0:7] data_out_3_phyest;
    wire  valid_out0_phy;
    wire  valid_out1_phy;
    wire  valid_out2_phy;
    wire  valid_out3_phy;
    wire  valid_out0_phyest;
    wire  valid_out1_phyest;
    wire  valid_out2_phyest;
    wire  valid_out3_phyest;


tx_rx p1 (/*AUTOINST*/
	  // Outputs
	  .data_out_0_phy		(data_out_0_phy),
	  .data_out_1_phy		(data_out_1_phy),
	  .data_out_2_phy		(data_out_2_phy),
	  .valid_out0_phy		(valid_out0_phy),
	  .valid_out1_phy		(valid_out1_phy),
	  .valid_out2_phy		(valid_out2_phy),
	  .valid_out3_phy		(valid_out3_phy),
	  .data_out_3_phy		(data_out_3_phy),
	  // Inputs
	  .clk_f			(clk_f),
	  .clk_2f			(clk_2f),
	  .clk_4f			(clk_4f),
	  .clk_32f			(clk_32f),
	  .reset			(reset),
	  .data_in0_phy			(data_in0_phy),
	  .data_in1_phy			(data_in1_phy),
	  .data_in2_phy			(data_in2_phy),
	  .data_in3_phy			(data_in3_phy),
	  .valid0_phy			(valid0_phy),
	  .valid1_phy			(valid1_phy),
	  .valid2_phy			(valid2_phy),
	  .valid3_phy			(valid3_phy),
	  .selectorRe			(selectorRe));

tx_rx_synth synth(/*AUTOINST*/
		  // Outputs
		  .data_out_0_phy	(data_out_0_phyest),
		  .data_out_1_phy	(data_out_1_phyest),
		  .data_out_2_phy	(data_out_2_phyest),
		  .data_out_3_phy	(data_out_3_phyest),
		  .valid_out0_phy	(valid_out0_phyest),
		  .valid_out1_phy	(valid_out1_phyest),
		  .valid_out2_phy	(valid_out2_phyest),
		  .valid_out3_phy	(valid_out3_phyest),
		  // Inputs
		  .clk_2f		(clk_2f),
		  .clk_32f		(clk_32f),
		  .clk_4f		(clk_4f),
		  .clk_f		(clk_f),
		  .data_in0_phy		(data_in0_phy),
		  .data_in1_phy		(data_in1_phy),
		  .data_in2_phy		(data_in2_phy),
		  .data_in3_phy		(data_in3_phy),
		  .reset		(reset),
		  .selectorRe		(selectorRe),
		  .valid0_phy		(valid0_phy),
		  .valid1_phy		(valid1_phy),
		  .valid2_phy		(valid2_phy),
		  .valid3_phy		(valid3_phy));
phy_probador prob (/*AUTOINST*/
		   // Outputs
		   .clk_f		(clk_f),
		   .clk_2f		(clk_2f),
		   .clk_4f		(clk_4f),
		   .clk_32f		(clk_32f),
		   .reset		(reset),
		   .data_in0_phy	(data_in0_phy),
		   .data_in1_phy	(data_in1_phy),
		   .data_in2_phy	(data_in2_phy),
		   .data_in3_phy	(data_in3_phy),
		   .valid1_phy		(valid1_phy),
		   .valid2_phy		(valid2_phy),
		   .valid3_phy		(valid3_phy),
		   .valid0_phy		(valid0_phy),
		   // Inputs
		   .data_out_0_phy	(data_out_0_phy),
		   .data_out_1_phy	(data_out_1_phy),
		   .data_out_2_phy	(data_out_2_phy),
		   .valid_out0_phy	(valid_out0_phy),
		   .valid_out1_phy	(valid_out1_phy),
		   .valid_out2_phy	(valid_out2_phy),
		   .valid_out3_phy	(valid_out3_phy),
		   .data_out_3_phy	(data_out_3_phy));
endmodule
