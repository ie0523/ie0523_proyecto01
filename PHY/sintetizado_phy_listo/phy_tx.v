`include "MuxL1.v"
`include "MuxL2.v"
`include "paralelo_serial.v"
`include "Recirculacion.v"
`include "serial_paralelo_idle.v"
`include "etapa_flops2.v"
`include "etapa_flops4.v"


module phy_tx (
    input clk_f, 
    input clk_2f, 
    input clk_4f, 
    input clk_32f,
    output data_out_tx,
    input [7:0] data_in0_tx, 
    input [7:0] data_in1_tx,
    input [7:0] data_in2_tx,
    //input paralelo_serial,
    
    input reset,
    input entrada_serial_paralelo,
   //input selectorRe,
    input valid0_tx,
    input valid1_tx,
    input valid2_tx,
    input valid3_tx,
    input [7:0] data_in3_tx
    );

    wire validout0f;
    wire validout1f;
    wire validout2f;
    wire validout3f;
    wire validout0f2;
     wire validout1f2;
      wire validout2f2;
       wire validout3f2;
    wire [7:0] outf0;
    wire [7:0] outf1;
    wire [7:0] outf2;
    wire [7:0] outf3;
    wire [7:0] outf0_2;
    wire [7:0] outf1_2;
    wire [7:0] outf2_2;
    wire [7:0] outf3_2;
    wire [7:0] outmux0;
    wire [7:0] outmux1;
     wire [7:0] outmux2;
    wire [7:0] data_out_recirculacion0;
    wire [7:0] data_out_recirculacion1;
    wire [7:0] data_out_recirculacion2;
    wire [7:0] data_out_recirculacion3;
    wire [7:0] data_out_recirculacion4;
    wire [7:0] data_out_recirculacion5;
    wire [7:0] data_out_recirculacion6;
    wire [7:0] data_out_recirculacion7;
    wire valid_out_re0;
    wire valid_out_re1;
    wire valid_out_re2;
    wire valid_out_re3;
    wire valid_out_re4;
    wire valid_out_re5;
    wire valid_out_re6;
    wire valid_out_re7;
    wire validout0f3;
    wire validout1f3;
    wire [7:0] out0f3;
    wire [7:0] out1f3;
    wire valid_outmux2;
    wire data_out_txx;
    wire [7:0] IDLE_outsp;
    

    
etapa_flops_4 ff_1(    
                    .clk (clk_f),
                    .data0 (data_in0_tx),
                    .data1 (data_in1_tx),
                    .data2 (data_in2_tx),
                    .data3 (data_in3_tx),
                    .reset (reset),
                    .out0    (outf0),
                    .out1    (outf1),
                    .out2    (outf2),
                    .out3    (outf3),
                    .valid0  (valid0_tx),
                    .valid1  (valid1_tx),
                    .valid2  (valid2_tx),
                    .valid3  (valid3_tx),
                    .valid_out0   (validout0f),
                    .valid_out1    (validout1f),
                    .valid_out2    (validout2f),
                    .valid_out3    (validout3f)
                    );

Recirculacionmod Re_1 (
                    .clk_f (clk_f),
                    .selectorRe (validoutsp),
                    .data_in0   (outf0), 
                    .data_in1   (outf1),
                    .data_in2   (outf2),
                    .data_in3   (outf3),
                    .valid0     (validout0f),
                    .valid1     (validout1f),
                    .valid2     (validout2f),
                    .valid3     (validout3f),
                    .valid_out0re  (valid_out_re0),
                    .valid_out1re   (valid_out_re1), 
                    .valid_out2re   (valid_out_re2),
                    .valid_out3re   (valid_out_re3),
                    .valid_out4re    (valid_out_re4),
                    .valid_out5re   (valid_out_re5),
                    .valid_out6re  (valid_out_re6),
                    .valid_out7re   (valid_out_re7),
                    .data_out0   (data_out_recirculacion0),
                    .data_out1   (data_out_recirculacion1),
                    .data_out2   (data_out_recirculacion2),
                    .data_out3   (data_out_recirculacion3),
                    .data_out4   (data_out_recirculacion4),
                    .data_out5   (data_out_recirculacion5),
                    .data_out6   (data_out_recirculacion6),
                    .data_out7   (data_out_recirculacion7)
);


etapa_flops_4 ff_2(    
                    .clk (clk_f),
                    .data0 (data_out_recirculacion4),
                    .data1 (data_out_recirculacion5),
                    .data2 (data_out_recirculacion6),
                    .data3 (data_out_recirculacion7),
                    .reset (reset),
                    .out0    (outf0_2),
                    .out1    (outf1_2),
                    .out2    (outf2_2),
                    .out3    (outf3_2),
                    .valid0  (valid_out_re4),
                    .valid1  (valid_out_re5),
                    .valid2  (valid_out_re6),
                    .valid3  (valid_out_re7),
                    .valid_out0   (validout0f2),
                    .valid_out1    (validout1f2),
                    .valid_out2    (validout2f2),
                    .valid_out3    (validout3f2)
                    );

MuxL1 mux1(
    .data_in_mux_L1_0   (outf0_2),
    .data_in_mux_L1_1   (outf1_2),
    .data_in_mux_L1_2    (outf2_2),
    .data_in_mux_L1_3    (outf3_2),
    .clk_2f              (clk_2f),
    .clk_f               (clk_f),
    .valid0               (validout0f2), 
    .valid1               (validout1f2),
    .valid2               (validout2f2),
    .valid3               (validout3f2),
    .selectorautomatico    (~clk_f),
    .reset                 (reset),
    .valid_out0            (validout0mux1),
    .valid_out1            (validout1mux1),
    .data_out_mux_L1_0     (outmux0),
    .data_out_mux_L1_1     (outmux1)

);

etapa_flops_2 ff_3(
                    .data0_flop2 (outmux0),
                    .data1_flop2 (outmux1),
                    .valid0_flop2 (validout0mux1),
                    .valid1_flop2 (validout1mux1),
                    .validout0_flop2 (validout0f3),
                    .validout1_flop2 (validout1f3),
                    .reset         (reset),
                    .clk         (clk_2f),
                    .out0_flop2     (out0f3),
                    .out1_flop2     (out1f3)
);

MuxL2 mux2(
    .data_in_mux_L2_0 (out0f3),
    .data_in_mux_L2_1  (out1f3),
    .clk_4f        (clk_4f),
    .valid_out      (valid_outmux2),
    .valid0        (validout0f3),
    .valid1        (validout1f3),
    .selectorautomatico (~clk_2f),
    .reset          (reset),
    .data_out_mux_L2_0  (outmux2)
);

paralelo_serial ps(  .clk_4f  (clk_4f),
	                     .clk_32f (clk_32f),
	                     .data_in_paralelo_8bit (outmux2),
	                    .valid (valid_outmux2),
	                    .reset (reset),
	                    .data_out_serial_1bit (data_out_txx)
    );


flop2mod fff( .clk  (clk_32f),
           .reset (reset),
           .datainflop (data_out_txx),
           .dataoutflop (data_out_tx)
);



serial_paralelo_idle cajita8(  .clk_4f (clk_4f),
	                       .clk_32f (clk_32f),
	                       .data_in_1bit (entrada_serial_paralelo),
	                       .reset (reset),
                           .validout (validoutsp),
                           .active (activesp),
	                       .IDLE_out (IDLE_outsp)
);

endmodule
