module serial_paralelo_idle (
  input		 clk_4f,
	input		 clk_32f,
	input 	data_in_1bit,
	input 		 reset,
  output reg   validout,
  output reg   active,
	output reg [0:7]	 IDLE_out
);

reg [0:2] contador;
reg [0:2] contadorBC;
reg [0:7] aux;
reg [0:7] guardado;



always @(posedge clk_32f)begin
    if (reset==0) begin
      contadorBC<=0;
      contador<=0;
      aux<=0;
      IDLE_out<=0;
      active<=0;

    end

    else begin

      aux[contador]<=data_in_1bit;
      contador<=contador+1;
        if (contador==0) begin
          IDLE_out<=aux;
          guardado<=0;
          if (aux==8'hBC) begin
            IDLE_out<=0;
            guardado<=8'hBC;
            contadorBC<= contadorBC+1;
              if (contadorBC==3)begin
                active<=1;
                IDLE_out<=data_in_1bit;
              end
          end
        end


    end

  end
always @ ( * ) begin
    validout = 0;
    if (guardado!=8'hBC &&  active==1)
      validout=1;
    else
      validout=0;
  end



endmodule
