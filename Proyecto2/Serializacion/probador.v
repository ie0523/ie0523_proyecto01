module probador(output reg clk_4f,
output reg clk_32f,
output reg valid1,
output reg valid2,
output reg [7:0] 	in_paralela1,
output reg [7:0] 	in_paralela2,
output reg	 reset,
input	 data_out_ps1,
input	 data_out_ps1_synth,
input	 data_out_ps2,
input	 data_out_ps2_synth
);


initial begin
    $dumpfile("serializacion.vcd");
    $dumpvars;
	in_paralela1<=0;//Se inicializan las entradas en 0
    in_paralela2<=0;
    reset<=0;
    valid1<=0;
    valid2<=0;
    @(posedge clk_4f);
        reset<=0;
    @(posedge clk_4f);
        reset<=0;
    @(posedge clk_4f);
        reset<=1;
    @(posedge clk_4f);
              valid1<=1;
              valid2<=1;

    in_paralela1<=8'hFF;
    in_paralela2<=8'hEE;
      @(posedge clk_4f);
          valid1<=1;
          valid2<=1;

    in_paralela1<=8'hEE;
    in_paralela2<=8'hFF;
     @(posedge clk_4f);
    valid1<=1;
    valid2<=1;
    in_paralela1<=8'hFF;
    in_paralela2<=8'h0F;
     @(posedge clk_4f);
    valid1<=0;
    valid2<=0;
    in_paralela1<=8'h0F;
    in_paralela2<=8'hFF;
     @(posedge clk_4f);
    valid1<=1;
    valid2<=1;
    in_paralela1<=8'h0E;
    in_paralela2<=8'h0E;
     @(posedge clk_4f);
    valid1<=0;
    valid2<=0;
    in_paralela1<=8'hF0;
    in_paralela2<=8'hF0;
    $finish;
end

initial clk_4f <= 0; 
always #64 clk_4f <= ~clk_4f;

initial clk_32f <= 1; 
always #8 clk_32f <= ~clk_32f;


endmodule
