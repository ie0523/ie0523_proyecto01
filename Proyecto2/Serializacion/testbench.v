`timescale 	1ns	/ 100ps
`include "probador.v"
`include "cmos_cells.v"
`include "synth.v"
`include "serializacion.v"

module testbench;
    wire		         clk_4f;
	wire		         clk_32f;
	wire [0:7]           in_paralela1;
	wire [0:7]           in_paralela2;
	wire		         valid1;
	wire		         valid2;
	wire 		         reset;
	wire	             data_out_ps1;
	wire	             data_out_ps2;
	wire	             data_out_ps1_synth;
	wire	             data_out_ps2_synth;

serializacion ser_( .clk_4f (clk_4f),
                    .clk_32f (clk_32f),
                    .in_paralela1 (in_paralela1),
                    .in_paralela2 (in_paralela2),
                    .reset (reset),
                    .valid1 (valid1),
                    .valid2 (valid2),
                    .data_out_ps1 (data_out_ps1),
                    .data_out_ps2 (data_out_ps2)
);
serializacion_synth ser_synth( .clk_4f (clk_4f),
                    .clk_32f (clk_32f),
                    .in_paralela1 (in_paralela1),
                    .in_paralela2 (in_paralela2),
                    .reset (reset),
                    .valid1 (valid1),
                    .valid2 (valid2),
                    .data_out_ps1 (data_out_ps1_synth),
                    .data_out_ps2 (data_out_ps2_synth)
);
probador prob( .clk_4f (clk_4f),
                    .clk_32f (clk_32f),
                    .in_paralela1 (in_paralela1),
                    .in_paralela2 (in_paralela2),
                    .reset (reset),
                    .valid1 (valid1),
                    .valid2 (valid2),
                    .data_out_ps1 (data_out_ps1),
                    .data_out_ps2 (data_out_ps2),
                    .data_out_ps1_synth (data_out_ps1_synth),
                    .data_out_ps2_synth (data_out_ps2_synth)
);
endmodule
