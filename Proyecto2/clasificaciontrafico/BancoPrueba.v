`include "probador.v"
`include "cmos_cells.v"
`include "trafico_synth.v"
`include "trafico.v"




module BancoPrueba;
    /*AUTOWIRE*/
    // Beginning of automatic wires (for undeclared instantiated-module outputs)
    wire		clk;			// From pb of probador.v
    wire [9:0]		data_out0;		// From trafico1 of trafico.v
    wire [9:0]		data_out1;		// From trafico1 of trafico.v
	wire [9:0]		data_out0est;		// From trafico1 of trafico.v
    wire [9:0]		data_out1est;		// From trafico1 of trafico.v
    wire		read;			// From pb of probador.v
    wire		reset_L;		// From pb of probador.v
    wire		write;			// From pb of probador.v
    // End of automatics
   wire [9:0] data_in;
    wire [4:0] full_TH, empty_TH;
    wire errorOverflow;
	wire almost_empty0, almost_full0,errorFull0, errorEmpty0;
	wire almost_empty1, almost_full1,errorFull1, errorEmpty1;
	wire almost_empty0est, almost_full0est,errorFull0est, errorEmpty0est;
	wire almost_empty1est, almost_full1est,errorFull1est, errorEmpty1est;

   trafico trafico1 (/*AUTOINST*/
		     // Outputs
		     .data_out0		(data_out0),
		     .errorFull0		(errorFull0),
		     .errorEmpty0	(errorEmpty0),
		     .almost_empty0	(almost_empty0),
		     .almost_full0	(almost_full0),
			 .errorFull1		(errorFull1),
		     .errorEmpty1	(errorEmpty1),
		     .almost_empty1	(almost_empty1),
		     .almost_full1	(almost_full1),
		     .data_out1		(data_out1),
		     // Inputs
		     .data_in		(data_in),
		     .write		(write),
		     .read		(read),
		     .full_TH		(full_TH),
		     .reset_L		(reset_L),
		     .empty_TH		(empty_TH),
		     .clk		(clk));

	   trafico_synt trafico_estr (/*AUTOINST*/
		     // Outputs
		     .data_out0		(data_out0est),
		     .errorFull0		(errorFull0est),
		     .errorEmpty0	(errorEmpty0est),
		     .almost_empty0	(almost_empty0est),
		     .almost_full0	(almost_full0est),
		     .data_out1		(data_out1est),
			 .errorFull1	(errorFull1est),
		     .errorEmpty1	(errorEmpty1est),
		     .almost_empty1	(almost_empty1est),
		     .almost_full1	(almost_full1est),
		     // Inputs
		     .data_in		(data_in),
		     .write		(write),
		     .read		(read),
		     .full_TH		(full_TH),
		     .reset_L		(reset_L),
		     .empty_TH		(empty_TH),
		     .clk		(clk));

    probador pb(/*AUTOINST*/
		     // Outputs
		     .data_out0		(data_out0),
		     .errorFull0		(errorFull0),
		     .errorEmpty0	(errorEmpty0),
		     .almost_empty0	(almost_empty0),
		     .almost_full0	(almost_full0),
		     .data_out1		(data_out1),
		     .errorEmpty1	(errorEmpty1),
		     .almost_empty1	(almost_empty1),
		     .almost_full1	(almost_full1),
		     // Inputs
		     .data_in		(data_in),
		     .write		(write),
		     .read		(read),
		     .full_TH		(full_TH),
		     .reset_L		(reset_L),
		     .empty_TH		(empty_TH),
		     .clk		(clk));

endmodule //test_mem;
