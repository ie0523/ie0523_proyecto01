module probador (
    input [9:0] data_out0,
    input [9:0] data_out1,
    input almost_empty0,
    input almost_full0,
    input errorFull0,
    input errorEmpty0,
    input almost_empty1,
    input almost_full1,
    input errorFull1,
    input errorEmpty1,
    output reg [9:0] data_in,
    output reg read,
    output reg write,
    output reg [4:0] empty_TH,
    output reg [4:0] full_TH,
    output reg clk,
    output reg reset_L);

    integer i, j;
    initial begin

        $dumpfile("trafico.vcd");
        $dumpvars;
        empty_TH <= 3;
        full_TH <= 6;
        data_in <= 0;
        write <= 0;
        read <= 0;
        reset_L <= 0;

        //  @(posedge clk)
        //     reset_L <= 0;
        // @(posedge clk)
        //     reset_L <= 1;

        // @(posedge clk) begin
        //     data_in <= 10'b1011101001;
        //     write <= 1;
        // end

        // @(posedge clk) begin
        //     data_in <= 10'b0101101011;
        
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b1011101010;
            
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b0101101110;

        // end


        // @(posedge clk) begin
        //     data_in <= 10'b1011101111;
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b0101101001;
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b1011101001;
        //     write <= 0;
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b0101101011;
          
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b1011101011;
        //     read <= 1;
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b0101101011;
           
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b1011101011;
          
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b0101101110;
           
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b1011101001;
            
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b0101101001;
          
        // end

        // @(posedge clk) begin
        //     data_in <= 10'b0111101001;
        //       read <= 0;
          
        // end

        // @(posedge clk) begin
        //     data_in <= 10'b0101101011;
        //     write <= 1;
    
        // end

        //         @(posedge clk) begin
        //     data_in <= 10'b0101101110;
           
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b1011101001;
            
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b0101101001;
          
        // end

        // @(posedge clk) begin
        //     data_in <= 10'b0111101001;
          
        // end

        // @(posedge clk) begin
        //     data_in <= 10'b0101101011;
    
        // end



        @(posedge clk)
            reset_L <= 0;
        @(posedge clk)
            reset_L <= 1;
        @(posedge clk) begin
            data_in <= $urandom%31;
            write <= 1;
        end

        for (i = 0; i < 12; i = i+1) begin
            @(posedge clk) begin
                data_in <= $urandom%31;
            end
        end
            write <= 0;
        @(posedge clk)
            read <= 1;
        repeat(8) begin
        @(posedge clk);
        end
        @(posedge clk)
            read <= 0;
        @(posedge clk) begin
            write <= 1;
        end
        @(posedge clk) begin
            data_in <= $urandom%31;
        end
        @(posedge clk) begin
            data_in <= $urandom%31;
        end
            read <= 1;
        for (i = 0; i < 15; i = i+1) begin
            @(posedge clk) begin
                data_in <= $urandom%31;
            end
        end

        $finish;
    end

    initial clk <= 0;
    always #1 clk <= ~clk;

endmodule
