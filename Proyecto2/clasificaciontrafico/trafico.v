`include "demux_1a2.v"
`include "fifo_cond.v"
`include "memoria_cond.v"
`include "read_Req.v"
`include "write_Req.v"
`include "flowControl.v"
`include "flipflop3b.v"
module trafico (
    input [9:0] data_in,
    output [9:0] data_out0,
    input write,
    input read,
    input [3:0] full_TH,
    input reset_L,
    input [3:0] empty_TH,
    input clk,
    output errorFull0,
    output errorEmpty0,
    output almost_empty0,
    output almost_full0,
    output errorFull1,
    output errorEmpty1,
    output almost_empty1,
    output almost_full1,
    output [9:0] data_out1
);

wire [9:0] a1;
wire [9:0] a2;

demux_conductual demux_1 (
      .clk  (clk),
      .data_in (data_in),
      .selector (data_in[0]),
      .reset  (reset_L),
      .data_out0 (a1),
      .data_out1 (a2)
);

fifo_8x10 fifo1
(                 .read_data_cond (data_out0),
                  .errorFull     (errorFull0),
                  .errorEmpty    (errorEmpty0),
                  .almost_empty   (almost_empty0),
                  .almost_full    (almost_full0),
                  // Input
                  .write     (write),
                  .read       (read),
                  .write_data (a1),
                  .full_TH    (full_TH),
                  .empty_TH   (empty_TH),
                  .clk        (clk),
                  .reset_L    (reset_L)

);

fifo_8x10 fifo2
(                 .read_data_cond (data_out1),
                  .errorFull     (errorFull1),
                  .errorEmpty    (errorEmpty1),
                  .almost_empty   (almost_empty1),
                  .almost_full    (almost_full1),
                  // Input
                  .write     (write),
                  .read       (read),
                  .write_data (a2),
                  .full_TH    (full_TH),
                  .empty_TH   (empty_TH),
                  .clk        (clk),
                  .reset_L    (reset_L)

);

endmodule