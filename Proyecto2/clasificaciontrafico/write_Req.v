module write_Req (
                    output reg [4:0] wr_dir,
                    output reg push,
                    input write,
                    input full,
                    input clk,
                    input reset_L);

  always @(posedge clk) begin
      if (!reset_L) begin
          wr_dir <= 0;
          push <= 0;
      end else begin
          // Revision de solicitud de escritura y estado no lleno
          if (write && !full) begin
              // Se activa senal de push para memoria
              push <= 1;
              // Se aumenta en 1 la direccion de memoria a escribit
              wr_dir <= wr_dir + 1;
              // Revisa si llega a la dir 7 para pasar a dir 0
              if (wr_dir == 7) wr_dir <= 0;
              // Si no se cumplen las condiciones, no activa senal push
          end else begin
              push <= 0;
          end
      end
  end

endmodule
