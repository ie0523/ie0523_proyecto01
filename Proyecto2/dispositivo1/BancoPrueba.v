`include "probador.v"
`include "cmos_cells.v"
`include "interconexion.v"




module BancoPrueba;
    
    wire		clk, clk_32f;	
	wire [7:0]  data_in;

    wire 		data_out_ps1;		
    wire 		data_out_ps2;			
    wire		read;			
    wire		reset_L;		
    wire		write;		
	wire class;
    wire dest;
    wire [4:0] full_TH1;
    wire [4:0] empty_TH1;
    wire [3:0] empty_TH2;
    wire [3:0] full_TH2;


   interconexion i1 (/*AUTOINST*/
		     // Inputs
		     .data_in		(data_in),
			.class           (class),
   			.dest            (dest),
   			.write           (write),
    		.read            (read),
    		.full_TH1        (full_TH1),
    		.reset_L         (reset_L),
    		.empty_TH1       (empty_TH1),
    		.empty_TH2       (empty_TH2),
   			.full_TH2        (full_TH2),
    		 .clk            (clk),
    		 .clk_32f        (clk_32f),
   	        .data_out_ps1    (data_out_ps1),
	        .data_out_ps2    (data_out_ps2)
			);
// /* 
//    interconexion_synth i2 (/*AUTOINST*/
// 		     // Inputs
// 		     .data_in		(data_in),
// 			.class           (class),
//    			.dest            (dest),
//    			.write           (write),
//     		.read            (read),
//     		.full_TH1        (full_TH1),
//     		.reset_L         (reset_L),
//     		.empty_TH1       (empty_TH1),
//     		.empty_TH2       (empty_TH2),
//    			.full_TH2        (full_TH2),
//     		 .clk            (clk),
//     		 .clk_32f        (clk_32f),
//    	        .data_out_ps1    (data_out_ps1),
// 	        .data_out_ps2    (data_out_ps2)
// 			);
//  */

   probador pb(/*AUTOINST*/
	       // Outputs
	       .data_in			(data_in),
		   	.class           (class),
   			.dest            (dest),
	       .read			(read),
	       .write			(write),
	    	.full_TH1        (full_TH1),
    		.reset_L         (reset_L),
    		.empty_TH1       (empty_TH1),
    		.empty_TH2       (empty_TH2),
   			.full_TH2        (full_TH2),
    		 .clk            (clk),
    		 .clk_32f        (clk_32f),

	       // Inputs
	       .data_out_ps1		(data_out_ps1),
	       .data_out_ps2		(data_out_ps2));

endmodule 
