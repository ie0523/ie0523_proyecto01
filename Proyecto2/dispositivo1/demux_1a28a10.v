module demux_conductual8a10 (
    input clk,
    input [9:0] data_in,
    input selector,
    input reset,
    output reg [9:0] data_out0,
    output reg [9:0] data_out1
);

reg [9:0] data_outtemp1;
reg [9:0] data_outtemp0;


always @ ( posedge clk ) begin
  if (reset==0) begin
    data_out1 <= 0;
    data_out0 <= 0;
  end
  else begin
    data_out1 <= data_outtemp1;
    data_out0 <= data_outtemp0;
    end
end



    always @(*) begin

      data_outtemp1=0;
      data_outtemp0=0;
     
    if(selector==0) begin
        data_outtemp0=data_in;
        data_outtemp1=data_out1;

    end else begin
        data_outtemp1=data_in;
        data_outtemp0=data_out0;
    end


end












endmodule













  //   end else if (validin == 0) begin
  //     valid_out0=0;
  //     valid_out1=0;
  //   end

  //   else begin
  //     if (selector==0) begin
  //       data_out0 = data_in;
  //       valid_out0=validin;
  //     end
  //     if (selector==1) begin
  //       data_out1=data_in;
  //       valid_out1=validin;
  //     end

  //   end

  // end
