`include "Mux2a1.v"
`include "demux_1a2.v"
`include "fifo_cond6a8.v"
`include "memoria_cond6a8.v"
`include "read_Req6a8.v"
`include "write_Req6a8.v"
`include "flowControl6a8.v"
`include "flipflop3b6a8.v"

module enrutamiento (
    input [9:0] data_in0,
    input [9:0] data_in1,
    output [7:0] data_out0,
    output [7:0] data_out1,
    input write,
    input read,
    input [3:0] full_TH,
    input [3:0] empty_TH,
    input clk,
    input reset,
    input selectorautomatico,
    output full0,
    output empty0,
    output almost_empty0,
    output almost_full0,
    output full1,
    output empty1,
    output almost_empty1,
    output almost_full1
);

wire [9:0] data_outmux;
wire [7:0] data_indemux;
wire dest;
assign data_indemux = data_outmux [7:0];
assign dest = data_outmux[8];
wire [7:0] salidaDemux0;
wire [7:0] salidaDemux1;


Mux2a1 mux1(
      .clk_f (clk),
			.data_inm0 (data_in0),
			.data_inm1 (data_in1),
			.data_outm (data_outmux),
			.valid0  (1'b1),
			.valid1  (1'b1),
      .selectorautomatico  (selectorautomatico),
      .reset  (reset),
			.valid_out  (valid_out)
);

demux_conductual demux_1 (
      .clk  (clk),
      .data_in (data_indemux),
      .selector (dest),
      .reset  (reset),
      .data_out0 (salidaDemux0),
      .data_out1 (salidaDemux1)
);


fifo_6x8 fifo0
(                 .read_data_cond (data_out0),
                  .full     (full0),
                  .empty    (empty0),
                  .almost_empty   (almost_empty0),
                  .almost_full    (almost_full0),
                  // Input
                  .write     (write),
                  .read       (read),
                  .write_data (salidaDemux0),
                  .full_TH    (full_TH),
                  .empty_TH   (empty_TH),
                  .clk        (clk),
                  .reset_L    (reset)

);

fifo_6x8 fifo1
(                 .read_data_cond (data_out1),
                  .full     (full1),
                  .empty    (empty1),
                  .almost_empty   (almost_empty1),
                  .almost_full    (almost_full1),
                  // Input
                  .write     (write),
                  .read       (read),
                  .write_data (salidaDemux1),
                  .full_TH    (full_TH),
                  .empty_TH   (empty_TH),
                  .clk        (clk),
                  .reset_L    (reset)

);



endmodule
