module fifo_8x10 (
                  // Output
                  output [9:0] read_data_cond,
                  output full,
                  output empty,
                  output almost_empty,
                  output almost_full,
                  // Input
                  input write,
                  input read,
                  input [9:0] write_data,
                  input [4:0] full_TH,
                  input [4:0] empty_TH,
                  input clk,
                  input reset_L);

wire [4:0] rd_dir, wr_dir, w_d, r_d;
wire pop, push;

memoria_cond8a10 memory0(/*AUTOINST*/
		    // Outputs
		    .read_data_cond	(read_data_cond[9:0]),
		    // Inputs
		    .rd_dir		(r_d[4:0]),
		    .wr_dir		(w_d[4:0]),
		    .write_data		(write_data[9:0]),
		    .memread		(pop),
		    .memwrite		(push),
		    .clk		(clk),
		    .reset_L		(reset_L));

read_Req8a10 read_rq0(/*AUTOINST*/
		 // Outputs
		 .rd_dir		(rd_dir[4:0]),
		 .pop			(pop),
		 // Inputs
		 .read			(read),
		 .empty			(empty),
		 .clk			(clk),
		 .reset_L		(reset_L));

write_Req8a10 write_rq0(/*AUTOINST*/
		   // Outputs
		   .wr_dir		(wr_dir[4:0]),
		   .push		(push),
		   // Inputs
		   .write		(write),
		   .full		(full),
		   .clk			(clk),
		   .reset_L		(reset_L));

flipflop3b8a10 write_dir_ff0 (/*AUTOINST*/
			 // Outputs
			 .Q			(w_d[4:0]),
			 // Inputs
			 .D			(wr_dir[4:0]),
			 .clk			(clk),
			 .reset_L		(reset_L));

flipflop3b8a10 read_dir_ff0 (/*AUTOINST*/
			// Outputs
			.Q		(r_d[4:0]),
			// Inputs
			.D		(rd_dir[4:0]),
			.clk		(clk),
			.reset_L	(reset_L));

flowControl8a10 flow_ctrl0(/*AUTOINST*/
		      // Outputs
		      .errorFull	(errorFull),
		      .errorEmpty	(errorEmpty),
		      .almost_empty	(almost_empty),
		      .almost_full	(almost_full),
		      .full		(full),
		      .empty		(empty),
		      // Inputs
		      .full_th		(full_TH[4:0]),
		      .empty_th		(empty_TH[4:0]),
		      .read		(read),
		      .write		(write),
		      .clk		(clk),
		      .reset_L		(reset_L));

endmodule
