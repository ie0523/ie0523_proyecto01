`include "enrutamiento.v"
`include "trafico.v"
`include "serializacion.v"

module interconexion (   
    input [7:0] data_in, 
    input class,
    input dest,
    input write,
    input read,
    input [4:0] full_TH1,
    input reset_L,
    input [4:0] empty_TH1,
    input [3:0] empty_TH2,
    input [3:0] full_TH2,
    input clk,
    input clk_32f,
    output 	            data_out_ps1,
	output 	            data_out_ps2
);

wire [9:0] data_outtraf0;
wire [9:0] data_outtraf1;

wire [7:0] data_outenr0;
wire [7:0] data_outenr1;


trafico traf1(
    .data_in   (data_in),
    .class     (class),
    .dest      (dest),
    .data_out0   (data_outtraf0),
    .write       (write),
    .read        (read),
    .full_TH     (full_TH1),
    .reset_L     (reset_L),
    .empty_TH    (empty_TH1),
    .clk          (clk),
    .full0   (full0),
    .empty0  (empty0),
    .almost_empty0 (almost_empty0),
    .almost_full0 (almost_full0),
    .full1  (full0),
    .empty1  (empty1),
    .almost_empty1 (almost_empty1),
    .almost_full1 (almost_full1),
    .data_out1  (data_outtraf1)

);


enrutamiento enr1(
    .data_in0   (data_outtraf0),
    .data_in1   (data_outtraf1),
    .data_out0  (data_outenr0),
    .data_out1  (data_outenr1),
    .write      (write),
    .read       (read),
    .full_TH    (full_TH2),
    .empty_TH   (empty_TH2),
    .clk   (clk),
    .reset (reset_L),
    .selectorautomatico (empty0),
    .full0    (full0en),
    .empty0 (empty0en),
    .almost_empty0 (almost_empty0en),
    .almost_full0 (almost_full0en),
    .full1 (full0en),
    .empty1 (empty1en),
    .almost_empty1 (almost_empty1en),
    .almost_full1  (almost_full1en)
);

serializacion s1 (
    .clk_4f (clk),
	.clk_32f (clk_32f),
	.in_paralela1 (data_outenr0),
	.in_paralela2 (data_outenr1),
	.valid1  (read),
	.valid2 (read),
	.data_out_ps1 (data_out_ps1),
	.data_out_ps2 (data_out_ps2),
	.reset (reset_L)
);

endmodule