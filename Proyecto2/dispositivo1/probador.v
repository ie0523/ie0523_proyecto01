module probador (
    input  data_out_ps1,
    input  data_out_ps2,
    output reg [7:0] data_in,
    output reg read,
    output reg write,
    output reg class,
    output reg dest,
    output reg [4:0] empty_TH1,
    output reg [4:0] full_TH1,
    output reg [3:0] empty_TH2,
    output reg [3:0] full_TH2,
    output reg clk,
    output reg clk_32f,
    output reg reset_L);

    integer i, j;
    initial begin

        $dumpfile("interconex.vcd");
        $dumpvars;
        empty_TH1 <= 3;
        full_TH1 <= 6;
        empty_TH2 <= 1;
        full_TH2 <= 5;
        data_in <= 0;
        write <= 0;
        read <= 0;
        reset_L <= 0;

        //  @(posedge clk)
        //     reset_L <= 0;
        // @(posedge clk)
        //     reset_L <= 1;

        // @(posedge clk) begin
        //     data_in <= 10'b1011101001;
        //     write <= 1;
        // end

        // @(posedge clk) begin
        //     data_in <= 10'b0101101011;
        
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b1011101010;
            
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b0101101110;

        // end


        // @(posedge clk) begin
        //     data_in <= 10'b1011101111;
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b0101101001;
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b1011101001;
        //     write <= 0;
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b0101101011;
          
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b1011101011;
        //     read <= 1;
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b0101101011;
           
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b1011101011;
          
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b0101101110;
           
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b1011101001;
            
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b0101101001;
          
        // end

        // @(posedge clk) begin
        //     data_in <= 10'b0111101001;
        //       read <= 0;
          
        // end

        // @(posedge clk) begin
        //     data_in <= 10'b0101101011;
        //     write <= 1;
    
        // end

        //         @(posedge clk) begin
        //     data_in <= 10'b0101101110;
           
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b1011101001;
            
        // end


        // @(posedge clk) begin
        //     data_in <= 10'b0101101001;
          
        // end

        // @(posedge clk) begin
        //     data_in <= 10'b0111101001;
          
        // end

        // @(posedge clk) begin
        //     data_in <= 10'b0101101011;
    
        // end



        @(posedge clk)
            reset_L <= 0;
        @(posedge clk)
            reset_L <= 1;
        @(posedge clk) begin
            data_in <= $urandom%31;
            dest <= $urandom%31;
            class <= $urandom%31;
            write <= 1;
        end

        for (i = 0; i < 12; i = i+1) begin
            @(posedge clk) begin
                data_in <= $urandom%31;
                dest <= $urandom%31;
               class <= $urandom%31;
            end
        end
            write <= 0;
        @(posedge clk)
            read <= 1;
        repeat(8) begin
        @(posedge clk);
        end
        @(posedge clk)
            read <= 0;
        @(posedge clk) begin
            write <= 1;
        end
        @(posedge clk) begin
            data_in <= $urandom%31;
                        dest <= $urandom%31;
            class <= $urandom%31;
        end
        @(posedge clk) begin
            data_in <= $urandom%31;
                        dest <= $urandom%31;
            class <= $urandom%31;
        end
            read <= 1;
        for (i = 0; i < 15; i = i+1) begin
            @(posedge clk) begin
                data_in <= $urandom%31;
                            dest <= $urandom%31;
            class <= $urandom%31;
            end
        end

        $finish;
    end

    initial clk <= 0;
    always #64 clk <= ~clk;

    initial clk_32f <= 1;
    always #8 clk_32f <= ~clk_32f;

endmodule
