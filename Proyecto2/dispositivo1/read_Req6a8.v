module read_Req (
                  output reg [3:0] rd_dir,
                  output reg pop,
                  input read,
                  input empty,
                  input clk,
                  input reset_L);

  always @(posedge clk) begin
      if (!reset_L) begin
          rd_dir <= 0;
          pop <= 0;
      end else begin
          // Revision de solicitud de lectura y estado no vacio
          if (read && !empty) begin
              // Se activa senal de pop para memoria
              pop <= 1;
              // Se aumenta en 1 la direccion de memoria a leer
              rd_dir <= rd_dir + 1;
              // Revisa si llega a la dir 7 para pasar a dir 0
              if (rd_dir == 5) begin
                  rd_dir <= 0;
              end
          end else begin
              // Si no se cumplen las condiciones, no activa senal pop
              pop <= 0;
          end
      end
  end

endmodule
