`include "demux_1a28a10.v"
`include "fifo_cond8a10.v"
`include "memoria_cond8a10.v"
`include "read_Req8a10.v"
`include "write_Req8a10.v"
`include "flowControl8a10.v"
`include "flipflop3b8a10.v"
module trafico (
    input [7:0] data_in,
    input class,
    input dest,
    output [9:0] data_out0,
    input write,
    input read,
    input [4:0] full_TH,
    input reset_L,
    input [4:0] empty_TH,
    input clk,
    output full0,
    output empty0,
    output almost_empty0,
    output almost_full0,
    output full1,
    output empty1,
    output almost_empty1,
    output almost_full1,
    output [9:0] data_out1
);
wire  [9:0] data_in10b;
assign data_in10b={class,dest,data_in};
wire [9:0] a1;
wire [9:0] a2;

demux_conductual8a10 demux_1 (
      .clk  (clk),
      .data_in (data_in10b),
      .selector (class),
      .reset  (reset_L),
      .data_out0 (a1),
      .data_out1 (a2)
);

fifo_8x10 fifo1
(                 .read_data_cond (data_out0),
                  .full     (full0),
                  .empty    (empty0),
                  .almost_empty   (almost_empty0),
                  .almost_full    (almost_full0),
                  // Input
                  .write     (write),
                  .read       (read),
                  .write_data (a1),
                  .full_TH    (full_TH),
                  .empty_TH   (empty_TH),
                  .clk        (clk),
                  .reset_L    (reset_L)

);

fifo_8x10 fifo2
(                 .read_data_cond (data_out1),
                  .full     (full1),
                  .empty    (empty1),
                  .almost_empty   (almost_empty1),
                  .almost_full    (almost_full1),
                  // Input
                  .write     (write),
                  .read       (read),
                  .write_data (a2),
                  .full_TH    (full_TH),
                  .empty_TH   (empty_TH),
                  .clk        (clk),
                  .reset_L    (reset_L)

);

endmodule