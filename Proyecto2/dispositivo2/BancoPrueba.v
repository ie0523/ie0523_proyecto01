`include "fifo_cond.v"
`include "probador.v"
`include "cmos_cells.v"
`include "fifo_est.v"
`include "memoria_cond.v"
`include "read_Req.v"
`include "write_Req.v"
`include "flowControl.v"
`include "flipflop3b.v"

module BancoPrueba;
   
    wire		clk;		
    wire		almost_empty_est;	
    wire		errorFull;		
    wire		read;			
    wire [4:0]		read_data_cond;		
    wire [4:0]		read_data_est;		
    wire		reset_L;		
    wire		write;			
    wire [4:0]		write_data;		// From pb of probador.v
    // End of automatics

    wire [3:0] full_TH, empty_TH;
    wire errorOverflow, errorEmpty, almost_empty, almost_full;

    fifo_cond conductual(/*AUTOINST*/
			 // Outputs
			 .read_data_cond	(read_data_cond[4:0]),
			 .errorFull		(errorFull),
			 .errorEmpty		(errorEmpty),
			 .almost_empty		(almost_empty),
			 .almost_full		(almost_full),
			 // Inputs
			 .write			(write),
			 .read			(read),
			 .write_data		(write_data[4:0]),
			 .full_TH		(full_TH[3:0]),
			 .empty_TH		(empty_TH[3:0]),
			 .clk			(clk),
			 .reset_L		(reset_L));

    fifo_est estructural(/*AUTOINST*/
			 // Outputs
			 .almost_empty_est	(almost_empty_est),
			 .errorEmpty		(errorEmpty),
			 .errorFull		(errorFull),
			 .almost_full		(almost_full),
			 .read_data_est		(read_data_est[4:0]),
			 // Inputs
			 .clk			(clk),
			 .empty_TH		(empty_TH[3:0]),
			 .full_TH		(full_TH[3:0]),
			 .read			(read),
			 .reset_L		(reset_L),
			 .write			(write),
			 .write_data		(write_data[4:0]));

    probador pb(/*AUTOINST*/
		// Outputs
		.write_data		(write_data[4:0]),
		.read			(read),
		.write			(write),
		.empty_TH		(empty_TH[3:0]),
		.full_TH		(full_TH[3:0]),
		.clk			(clk),
		.reset_L		(reset_L),
		// Inputs
		.read_data_cond		(read_data_cond[4:0]),
		.read_data_est		(read_data_est[4:0]),
		.almost_empty		(almost_empty),
		.almost_full		(almost_full),
		.errorFull		(errorFull),
		.errorEmpty		(errorEmpty));

endmodule //test_mem;
