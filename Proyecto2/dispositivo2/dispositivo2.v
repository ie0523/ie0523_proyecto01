`include "serializacion_disp2.v"
`include "fifo_cond4a8.v"


module dispositivo2 (
                  input		         clk_4f,
	              input		         clk_32f,
                  input                in_serial_1,
                  input                in_serial_2,
                  output            validout_1,
                  output           validout_2,
                  input                reset,
                  output            active,
                  output [7:0] dataout0,
                  output [7:0] dataout1,
                  output full,
                  output empty,
                  output almost_empty,
                  output almost_full,
                  input write,
                  input read,
                  input  [4:0] full_TH,
                  input  [4:0] empty_TH
);

wire  [7:0]out_8bit_1;
wire  [7:0]out_8bit_2;


serializacion_disp2 serialdisp2(
                    .clk_4f         (clk_4f),
                    .clk_32f        (clk_32f),
                    .in_serial_1    (in_serial_1),
                    .in_serial_2    (in_serial_2),
                    .validout_1     (validout_1),
                    .validout_2     (validout_2),
                    .reset          (reset),
                    .active         (active),
                    .out_8bit_1     (out_8bit_1), 
                    .out_8bit_2     (out_8bit_2)
);


fifo_cond4a8 fifo4a8disp2_1(
                    .read_data_cond     (dataout0),
                    .full          (full),
                    .empty         (empty),
                    .almost_empty       (almost_empty),
                    .almost_full        (almost_full),
                    .write              (write),
                    .read               (read),
                    .write_data         (out_8bit_1),
                    .full_TH            (full_TH),
                    .empty_TH           (empty_TH),
                    .clk                (clk_4f),    
                    .reset_L            (reset)
);

fifo_cond4a8 fifo4a8disp2_2(
                    .read_data_cond     (dataout1),
                    .full          (full),
                    .empty         (empty),
                    .almost_empty       (almost_empty),
                    .almost_full        (almost_full),
                    .write              (write),
                    .read               (read),
                    .write_data         (out_8bit_2),
                    .full_TH            (full_TH),
                    .empty_TH           (empty_TH),
                    .clk                (clk_4f),    
                    .reset_L            (reset)
);

endmodule

