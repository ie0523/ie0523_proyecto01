`include "memoria_cond4a8.v"
`include "read_Req4a8.v"
`include "write_Req4a8.v"
`include "flowControl4a8.v"
`include "flipflop3b4a8.v"
module fifo_cond4a8 (
                  // Output
                  output [7:0] read_data_cond,
                  output full,
                  output empty,
                  output almost_empty,
                  output almost_full,
                  // Input
                  input write,
                  input read,
                  input [7:0] write_data,
                  input [4:0] full_TH,
                  input [4:0] empty_TH,
                  input clk,
                  input reset_L
);

wire [2:0] rd_dir, wr_dir, w_d, r_d;

memoria_cond4a8 memory(/*AUTOINST*/
		    // Outputs
		    .read_data_cond	(read_data_cond),
		    // Inputs
		    .rd_dir		(r_d),
		    .wr_dir		(w_d),
		    .write_data		(write_data),
		    .memread		(pop),
		    .memwrite		(push),
		    .clk		(clk),
		    .reset_L		(reset_L));

read_Req4a8 read_rq(/*AUTOINST*/
		 // Outputs
		 .rd_dir		(rd_dir),
		 .pop			(pop),
		 // Inputs
		 .read			(read),
		 .empty			(empty),
		 .clk			(clk),
		 .reset_L		(reset_L));

write_Req4a8 write_rq(/*AUTOINST*/
		   // Outputs
		   .wr_dir		(wr_dir),
		   .push		(push),
		   // Inputs
		   .write		(write),
		   .full		(full),
		   .clk			(clk),
		   .reset_L		(reset_L));

flipflop3b4a8 write_dir_ff (/*AUTOINST*/
			 // Outputs
			 .Q			(w_d),
			 // Inputs
			 .D			(wr_dir),
			 .clk			(clk),
			 .reset_L		(reset_L));

flipflop3b4a8 read_dir_ff (/*AUTOINST*/
			// Outputs
			.Q		(r_d),
			// Inputs
			.D		(rd_dir),
			.clk		(clk),
			.reset_L	(reset_L));

flowControl4a8 flow_ctrl(/*AUTOINST*/
		      // Outputs
		      .errorFull	(errorFull),
		      .errorEmpty	(errorEmpty),
		      .almost_empty	(almost_empty),
		      .almost_full	(almost_full),
		      .full		(full),
		      .empty		(empty),
		      // Inputs
		      .full_th		(full_TH),
		      .empty_th		(empty_TH),
		      .read		(read),
		      .write		(write),
		      .clk		(clk),
		      .reset_L		(reset_L));

endmodule
