module flipflop3b4a8 (
                    output reg [2:0] Q,
                    input [2:0] D,
                    input clk,
                    input reset_L
);

  // Flip-flop de 4 bits
  always @(posedge clk) begin
  	if (!reset_L) begin
        Q <= 0;
    end else begin
        Q <= D;
    end
  end
endmodule
