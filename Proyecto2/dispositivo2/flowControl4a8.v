module flowControl4a8 (
                    output reg errorFull,
                    output reg errorEmpty,
                    output reg almost_empty,
                    output reg almost_full,
                    output reg full,
                    output reg empty,
                    input [4:0] full_th,
                    input [4:0] empty_th,
                    input read,
                    input write,
                    input clk,
                    input reset_L);

reg [2:0] data_count; // Contador de datos

always @ (posedge clk) begin
    if (!reset_L) begin
        data_count <= 0;
        errorFull <= 0;
        errorEmpty <= 0;
        almost_empty <= 0;
        almost_full <= 0;
        full <= 0;
        empty <= 0;
    end else begin

        if (data_count==0) begin
            empty<= 1;
        end
        // Revision de contador con umbral lleno
        if (data_count >= full_th) begin
            // Se activa alerta de lleno
            almost_full <= 1;
        end else begin
            // Se desactiva alerta de lleno
            almost_full <= 0;
        end
        // Revision de contador con umbral vacio
        if (data_count <= empty_th) begin
            // Se activa alerta de vacio
            almost_empty <= 1;
        end else begin
            // Se desactiva alerta de vacio
            almost_empty <= 0;
        end
        // Revision de contador en 0
        if (data_count == 0) begin
            // Revisa si se solicita lectura con contador en 0
            if (read) begin
                // Se activa errorEmpty
                errorEmpty <= 1;
            end else begin
                // Se desactiva errorEmpty
                errorEmpty <= 0;
            end
        // Revision de solicitud de lectura
        end else if (read) begin
            // Se disminuye en 1 el contador de datos
            data_count <= data_count - 1;
            // Se desactiva errorEmpty
            errorEmpty <= 0;
            // Revisa si se solicita lectura con contador en 1
            if (data_count == 1) begin
                // Se activa senal de vacio
                empty <= 1;
            // Revisa si se solicita lectura con contador menor a 8
            end else if (data_count <= 4) begin
                // Se desactiva senal de full
                full <= 0;
            end
        end
        // Revision de contador en 8
        if (data_count == 4) begin
            // Revisa si se solicita escritura con contador en 8
            if (write) begin
                // Se activa senal de errorFull
                errorFull <= 1;
            end else begin
                // Se desactiva semal de errorFull
                errorFull <= 0;
            end
        // Revision de escritura
        end else if (write) begin
            // Se aumenta en 1 el contador de datos
            data_count <= data_count + 1;
            // Se desactiva senal de errorFull
            errorFull <= 0;
            // Revisa si se solicita escritura con contador en 7
            if (data_count == 3) begin
                // Se activa senal de full
                full <= 1;
            // Revisa si se solicita escritura con contador en mayor a 0
            end else if (data_count >= 0) begin
                // Se desactiva senal de empty
                empty <= 0;
            end
        end
        // Revision de solicitud de escritura y lectura
        if (read && write) begin
            // Se mantiene contador de datos
            data_count <= data_count;
        end
    end
end

endmodule
