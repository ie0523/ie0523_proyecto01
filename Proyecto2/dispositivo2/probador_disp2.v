module probador_disp2 (
                  output reg		         clk_4f,
	              output reg		         clk_32f,
                  output reg                in_serial_1,
                  output reg                in_serial_2,
                  input           validout_1,
                  input           validout_2,
                  output reg                reset,
                  input           active,
	              input [0:7]	 dataout0,
	              input [0:7]	 dataout1,
                  input full,
                  input empty,
                  input almost_empty,
                  input almost_full,
                  output reg write,
                  output reg read,
                  output reg  [4:0] full_TH,
                  output reg  [4:0] empty_TH,
                  output reg clk,
                  output reg reset_L
);


initial begin
    $dumpfile("disp2.vcd");
    $dumpvars;
	in_serial_1<=0;//Se inicializan las entradas en 0
	in_serial_2<=0;//Se inicializan las entradas en 0
    empty_TH <= 1;
    full_TH <= 3;
    reset<=0;
    write<=0;
    read<=0;

    @(posedge clk_32f);
        reset<=0;
        reset_L<=0;
    @(posedge clk_32f);
        reset<=0;
        reset_L<=0;
    @(posedge clk_32f);
        reset<=0;
        reset_L<=0;
    @(posedge clk_32f);
        reset<=0;
        reset_L<=0;
    @(posedge clk_32f);
        reset<=0;
        reset_L<=0;
    @(posedge clk_32f);
        reset<=0;
        reset_L<=0;
    @(posedge clk_32f);
        reset<=0;
        reset_L<=0;
    @(posedge clk_32f);
        reset<=1;
        reset_L<=1;
        in_serial_1<=1;
        in_serial_2<=1;
///////////////////////////////



    @(posedge clk_32f);
        write<=1;
        in_serial_1<=0;
        in_serial_2<=0;
    @(posedge clk_32f);
        in_serial_1<=1;
        in_serial_2<=1;
    @(posedge clk_32f);
        in_serial_1<=1;
        in_serial_2<=1;
    @(posedge clk_32f);
        in_serial_1<=1;
        in_serial_2<=1;
    @(posedge clk_32f);
        in_serial_1<=1;
        in_serial_2<=1;
    @(posedge clk_32f);
        in_serial_1<=0;
        in_serial_2<=0;
    @(posedge clk_32f);
        in_serial_1<=0;
        in_serial_2<=0;


                ///////////////////////////////
                @(posedge clk_32f);
                    in_serial_1<=1;
                    in_serial_2<=1;

                @(posedge clk_32f);
                    in_serial_1<=0;
                    in_serial_2<=0;

                @(posedge clk_32f);
                    in_serial_1<=1;
                    in_serial_2<=1;
                @(posedge clk_32f);
                    in_serial_1<=1;
                    in_serial_2<=1;
                @(posedge clk_32f);
                    in_serial_1<=1;
                    in_serial_2<=1;
                @(posedge clk_32f);
                    in_serial_1<=1;
                    in_serial_2<=1;
                @(posedge clk_32f);
                    in_serial_1<=0;
                    in_serial_2<=0;
                @(posedge clk_32f);
                    in_serial_1<=0;
                    in_serial_2<=0;
                    ///////////////////////////////
                    @(posedge clk_32f);
                        in_serial_1<=1;
                        in_serial_2<=1;

                    @(posedge clk_32f);
                        in_serial_1<=0;
                        in_serial_2<=0;

                    @(posedge clk_32f);
                        in_serial_1<=1;
                        in_serial_2<=1;
                    @(posedge clk_32f);
                        in_serial_1<=1;
                        in_serial_2<=1;
                    @(posedge clk_32f);
                        in_serial_1<=1;
                        in_serial_2<=1;
                    @(posedge clk_32f);
                        in_serial_1<=1;
                        in_serial_2<=1;
                    @(posedge clk_32f);
                        in_serial_1<=0;
                        in_serial_2<=0;
                    @(posedge clk_32f);
                        in_serial_1<=0;
                        in_serial_2<=0;
                        ///////////////////////////////
                        @(posedge clk_32f);
                            in_serial_1<=1;
                            in_serial_2<=1;

                        @(posedge clk_32f);
                            in_serial_1<=0;
                            in_serial_2<=0;

                        @(posedge clk_32f);
                            in_serial_1<=1;
                            in_serial_2<=1;
                        @(posedge clk_32f);
                            in_serial_1<=1;
                            in_serial_2<=1;

                        @(posedge clk_32f);
                            in_serial_1<=1;
                            in_serial_2<=1;
                        @(posedge clk_32f);
                            in_serial_1<=1;
                            in_serial_2<=1;
                        @(posedge clk_32f);
                            in_serial_1<=0;
                            in_serial_2<=0;
                        @(posedge clk_32f);
                            in_serial_1<=0;
                            in_serial_2<=0;
        ////////////////////////////////
        @(posedge clk_32f);
            in_serial_1<=1;
            write<=1;
            @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;

                @(posedge clk_32f);
                in_serial_2<=1;
                in_serial_1<=1;
                @(posedge clk_32f);
                in_serial_2<=1;
                in_serial_1<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_2<=1;
                in_serial_1<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;


                ////
                @(posedge clk_32f);
                read<=1;
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_2<=0;
                in_serial_1<=0;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=0;
                in_serial_2<=0;
                //
                @(posedge clk_32f);
                in_serial_2<=1;
                in_serial_1<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                //
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;

//////////////////////////////////////////////
                @(posedge clk_32f);
                read<=1;
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_2<=0;
                in_serial_1<=0;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=0;
                in_serial_2<=0;
                //
                @(posedge clk_32f);
                in_serial_2<=1;
                in_serial_1<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                //
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                ////
                @(posedge clk_32f);
                read<=1;
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_2<=0;
                in_serial_1<=0;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=0;
                in_serial_2<=0;
                //
                @(posedge clk_32f);
                in_serial_2<=1;
                in_serial_1<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                //
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;

//////////////////////////////////////////////
                @(posedge clk_32f);
                read<=1;
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_2<=0;
                in_serial_1<=0;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=0;
                in_serial_2<=0;
                //
                @(posedge clk_32f);
                in_serial_2<=1;
                in_serial_1<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                //
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
                @(posedge clk_32f);
                in_serial_1<=1;
                in_serial_2<=1;
    $finish;
end

initial clk_4f <= 0;
always #32 clk_4f <= ~clk_4f;

initial clk_32f <= 1;
always #4 clk_32f <= ~clk_32f;


endmodule
