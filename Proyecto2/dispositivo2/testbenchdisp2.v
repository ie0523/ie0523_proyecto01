`include "probador_disp2.v"
`include "cmos_cells.v"
`include "dispositivo2.v"


module testbenchdisp2;
                  wire		            clk_4f;
	              wire		            clk_32f;
                  wire                  in_serial_1;
                  wire                  in_serial_2;
                  wire                  validout_1;
                  wire                  validout_2;
                  wire                  reset;
                  wire                  active;
                  wire [7:0]            dataout0;
				  wire [7:0]            dataout1;
                  wire                  full;
                  wire                  empty;
                  wire                  almost_empty;
                  wire                  almost_full;
                  wire                  write;
                  wire                  read;
                  wire [4:0]            full_TH;
                  wire [4:0]            empty_TH;
                  wire                  clk;
                  wire                  reset_L;



dispositivo2 disp2 ( /*AUTOINST*/
		    // Outputs
		    .validout_1		(validout_1),
		    .validout_2		(validout_2),
		    .active		    (active),
			.dataout0        (dataout0),
			.dataout1        (dataout1),
		    .full		(full),
		    .empty		(empty),
		    .almost_empty	(almost_empty),
		    .almost_full	(almost_full),
		    // Inputs
		    .clk_4f		    (clk_4f),
		    .clk_32f		(clk_32f),
		    .in_serial_1	(in_serial_1),
		    .in_serial_2	(in_serial_2),
		    .reset		    (reset),
		    .write		    (write),
		    .read		    (read),
		    .full_TH		(full_TH),
		    .empty_TH		(empty_TH));


probador_disp2 pbdisp2 ( /*AUTOINST*/
		    // Outputs
		    .validout_1		(validout_1),
		    .validout_2		(validout_2),
		    .active		    (active),
 			.dataout0        (dataout0),
			.dataout1        (dataout1),           
	
		    .full		(full),
		    .empty		(empty),
		    .almost_empty	(almost_empty),
		    .almost_full	(almost_full),
		    // Inputs
		    .clk_4f		    (clk_4f),
		    .clk_32f		(clk_32f),
		    .in_serial_1	(in_serial_1),
		    .in_serial_2	(in_serial_2),
		    .reset		    (reset),
		    .write		    (write),
		    .read		    (read),
		    .full_TH		(full_TH),
		    .empty_TH		(empty_TH),
		    .reset_L		(reset_L)


);


endmodule
