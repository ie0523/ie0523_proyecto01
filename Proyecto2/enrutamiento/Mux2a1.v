module Mux2a1 (
    input clk_f,
    input [9:0] data_inm0,
    input [9:0] data_inm1,
    input valid0,
    input valid1,
    input selectorautomatico,
    input reset,
    output reg valid_out,
    output reg [9:0] data_outm
);

reg [9:0] a;
reg validtemp;



    always @(*) begin
        a = 0; //valor por defecto
        validtemp = 0; //valor por defecto

    if (selectorautomatico==0) begin
        validtemp = valid0;
        if (valid0)
            a = data_inm0;
        else
            a = data_outm;
        end

    else begin
        validtemp = valid1;
        if (valid1)
            a = data_inm1;
        else
            a = data_outm;
        end

    end

    always @ ( posedge clk_f ) begin
    valid_out<=validtemp;
      if (reset==1) begin
        data_outm <= a;
      end
      else begin
        data_outm <= 0;
      end

    end
endmodule
