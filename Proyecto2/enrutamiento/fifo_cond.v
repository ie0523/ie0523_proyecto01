module fifo_6x8 (
                  // Output
                  output [7:0] read_data_cond,
                  output errorFull,
                  output errorEmpty,
                  output almost_empty,
                  output almost_full,
                  // Input
                  input write,
                  input read,
                  input [7:0] write_data,
                  input [3:0] full_TH,
                  input [3:0] empty_TH,
                  input clk,
                  input reset_L);

wire [3:0] rd_dir, wr_dir, w_d, r_d;

memoria_cond memory(/*AUTOINST*/
		    // Outputs
		    .read_data_cond	(read_data_cond[7:0]),
		    // Inputs
		    .rd_dir		(r_d[3:0]),
		    .wr_dir		(w_d[3:0]),
		    .write_data		(write_data[7:0]),
		    .memread		(pop),
		    .memwrite		(push),
		    .clk		(clk),
		    .reset_L		(reset_L));

read_Req read_rq(/*AUTOINST*/
		 // Outputs
		 .rd_dir		(rd_dir[3:0]),
		 .pop			(pop),
		 // Inputs
		 .read			(read),
		 .empty			(empty),
		 .clk			(clk),
		 .reset_L		(reset_L));

write_Req write_rq(/*AUTOINST*/
		   // Outputs
		   .wr_dir		(wr_dir[3:0]),
		   .push		(push),
		   // Inputs
		   .write		(write),
		   .full		(full),
		   .clk			(clk),
		   .reset_L		(reset_L));

flipflop3b write_dir_ff (/*AUTOINST*/
			 // Outputs
			 .Q			(w_d[3:0]),
			 // Inputs
			 .D			(wr_dir[3:0]),
			 .clk			(clk),
			 .reset_L		(reset_L));

flipflop3b read_dir_ff (/*AUTOINST*/
			// Outputs
			.Q		(r_d[3:0]),
			// Inputs
			.D		(rd_dir[3:0]),
			.clk		(clk),
			.reset_L	(reset_L));

flowControl flow_ctrl(/*AUTOINST*/
		      // Outputs
		      .errorFull	(errorFull),
		      .errorEmpty	(errorEmpty),
		      .almost_empty	(almost_empty),
		      .almost_full	(almost_full),
		      .full		(full),
		      .empty		(empty),
		      // Inputs
		      .full_th		(full_TH[3:0]),
		      .empty_th		(empty_TH[3:0]),
		      .read		(read),
		      .write		(write),
		      .clk		(clk),
		      .reset_L		(reset_L));

endmodule
