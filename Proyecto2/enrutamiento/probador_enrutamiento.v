module probador (
    input [7:0] data_out0,
    input [7:0] data_out1,
    //input [4:0] read_data_est,
    output reg [9:0] data_in0,
    output reg [9:0] data_in1,
    output reg read,
    output reg write,
    output reg [3:0] empty_TH,
    output reg [3:0] full_TH,
    output reg clk,
    output reg selectorautomatico,
    output reg reset);

    integer i, j;
    initial begin

        $dumpfile("enrutamiento.vcd");
        $dumpvars;
        empty_TH <= 1;
        full_TH <= 5;
        data_in0 <= 10'b1011101111;
        data_in1 <= 10'b1111111011;
        write <= 0;
        read <= 0;
        reset <= 0;
        selectorautomatico=0;



        @(posedge clk)
            reset <= 0;
        @(posedge clk)
            reset <= 1;
        @(posedge clk) begin
                data_in0 <= $urandom%31;
                data_in1 <= $urandom%31;
            write <= 1;
        end

        for (i = 0; i < 12; i = i+1) begin
            @(posedge clk) begin
                data_in0 <= $urandom%31;
                data_in1 <= $urandom%31;
            end
        end
            write <= 0;
        @(posedge clk)
            read <= 1;
        repeat(6) begin
        @(posedge clk);
        end
        @(posedge clk)
            read <= 0;
        @(posedge clk) begin
            write <= 1;
            selectorautomatico=1;
        end
        @(posedge clk) begin
                data_in0 <= $urandom%31;
                data_in1 <= $urandom%31;
        end
        @(posedge clk) begin
                data_in0 <= $urandom%31;
                data_in1 <= $urandom%31;
             
        end
            read <= 1;
        for (i = 0; i < 15; i = i+1) begin
            @(posedge clk) begin
                data_in0 <= $urandom%31;
                data_in1 <= $urandom%31;
                    selectorautomatico=0;
            end
        end

/* 
        @(posedge clk)
            reset <= 0;
        @(posedge clk)
            reset <= 1;

        @(posedge clk) begin
            data_in0 <= 10'b1011101001;
            selectorautomatico=0;
            write <= 1;
        end


        @(posedge clk) begin
            data_in1 <= 10'b0101101011;
            selectorautomatico=0;
        end


        @(posedge clk) begin
            data_in0 <= 10'b1011101010;
            selectorautomatico=0;
        end


        @(posedge clk) begin
            data_in1 <= 10'b0101101110;
            selectorautomatico=0;
        end


        @(posedge clk) begin
            data_in0 <= 10'b1011101111;
            selectorautomatico=0;
        end


        @(posedge clk) begin
            data_in1 <= 10'b0101101001;
            selectorautomatico=0;
        end


        @(posedge clk) begin
            data_in0 <= 10'b1011101001;
            selectorautomatico=0;
            write <= 0;
        end


        @(posedge clk) begin
            data_in1 <= 10'b0101101011;
            selectorautomatico=0;
        end


        @(posedge clk) begin
            data_in0 <= 10'b1011101011;
            selectorautomatico=0;
            read <= 1;
        end


        @(posedge clk) begin
            data_in1 <= 10'b0101101011;
            selectorautomatico=1;
        end


        @(posedge clk) begin
            data_in0 <= 10'b1011101011;
            selectorautomatico=1;
        end


        @(posedge clk) begin
            data_in1 <= 10'b0101101110;
            selectorautomatico=1;
        end


        @(posedge clk) begin
            data_in0 <= 10'b1011101001;
            selectorautomatico=1;
        end


        @(posedge clk) begin
            data_in1 <= 10'b0101101001;
            selectorautomatico=1;
        end

        @(posedge clk) begin
            data_in1 <= 10'b0111101001;
            selectorautomatico=0;
        end

        @(posedge clk) begin
            data_in1 <= 10'b0101101011;
            selectorautomatico=0;
        end


@(posedge clk) begin
            data_in0 <= 10'b1011101001;
            selectorautomatico=0;
            write <= 1;
        end


        @(posedge clk) begin
            data_in1 <= 10'b0101101011;
            selectorautomatico=0;
        end


        @(posedge clk) begin
            data_in0 <= 10'b1011101010;
            selectorautomatico=0;
        end


        @(posedge clk) begin
            data_in1 <= 10'b0101101110;
            selectorautomatico=0;
        end


        @(posedge clk) begin
            data_in0 <= 10'b1011101111;
            selectorautomatico=0;
        end


        @(posedge clk) begin
            data_in1 <= 10'b0101101001;
            selectorautomatico=0;
        end


        @(posedge clk) begin
            data_in0 <= 10'b1011101001;
            selectorautomatico=0;
            write <= 0;
        end


        @(posedge clk) begin
            data_in1 <= 10'b0101101011;
            selectorautomatico=0;
        end


        @(posedge clk) begin
            data_in0 <= 10'b1011101011;
            selectorautomatico=0;
            read <= 1;
        end


        @(posedge clk) begin
            data_in1 <= 10'b0101101011;
            selectorautomatico=0;
        end

        @(posedge clk) begin
            data_in0 <= 10'b1011101001;
            selectorautomatico=0;
            write <= 0;
        end


        @(posedge clk) begin
            data_in1 <= 10'b0101101011;
            selectorautomatico=0;
        end


        @(posedge clk) begin
            data_in0 <= 10'b1011101011;
            selectorautomatico=0;
            read <= 1;
        end


        @(posedge clk) begin
            data_in1 <= 10'b0101101011;
            selectorautomatico=0;
        end
 */


        $finish;
    end

    initial clk <= 0;
    always #20 clk <= ~clk;

endmodule
