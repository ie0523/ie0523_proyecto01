`include "enrutamiento_synth.v"
`include "enrutamiento.v"
`include "probador_enrutamiento.v"
`include "cmos_cells.v"
//`include "fifo_est.v"
/*`include "memoria_cond.v"
`include "read_Req.v"
`include "write_Req.v"
`include "flowControl.v"
`include "flipflop3b.v"
`include "Mux2a1.v"
`include "demux_1a2.v"
*/

module BancoPrueba;

    wire almost_empty;
    wire  almost_full;
    wire  errorFull;
    wire  errorEmpty;
    wire  [9:0] data_in0;
    wire  [9:0] data_in1;
    wire  read;
    wire  write;
    wire  [3:0] empty_TH;
    wire  [3:0] full_TH;
    wire  clk;
    wire  selectorautomatico;
    wire  reset;
    wire [7:0] data_out0;
    wire [7:0] data_out1;
    wire [7:0] data_out0est;
    wire [7:0] data_out1est;
    wire  push;
    wire pop;

    // End of automatics



    enrutamiento er1(/*AUTOINST*/
           .data_in0 (data_in0),
           .data_in1 (data_in1),
           .data_out0 (data_out0),
           .data_out1 (data_out1),
           .write (write),
           .read (read),
           .full_TH (full_TH),
           .empty_TH (empty_TH),
           .clk (clk),
           .reset (reset),
           .selectorautomatico (selectorautomatico),
           .errorFull0 (errorFull0),
           .errorEmpty0 (errorEmpty0),
           .almost_empty0 (almost_empty0),
           .almost_full0 (almost_full0),
           .errorFull1 (errorFull1),
           .errorEmpty1 (errorEmpty1),
           .almost_empty1 (almost_empty1),
           .almost_full1 (almost_full1)
       );

    probador pb (
      .data_out0 (data_out0),
      .data_out1 (data_out1),
      .data_in0 (data_in0),
      .data_in1 (data_in1),
      .read (read),
      .write (write),
      .empty_TH (empty_TH),
      .full_TH (full_TH),
      .clk (clk),
      .selectorautomatico (selectorautomatico),
      .reset (reset)

      );

        enrusyn er2(/*AUTOINST*/
           .data_in0 (data_in0),
           .data_in1 (data_in1),
           .data_out0 (data_out0est),
           .data_out1 (data_out1est),
           .write (write),
           .read (read),
           .full_TH (full_TH),
           .empty_TH (empty_TH),
           .clk (clk),
           .reset (reset),
           .selectorautomatico (selectorautomatico),
           .errorFull0 (errorFull0est),
           .errorEmpty0 (errorEmpty0est),
           .almost_empty0 (almost_empty0est),
           .almost_full0 (almost_full0est),
           .errorFull1 (errorFull1est),
           .errorEmpty1 (errorEmpty1est),
           .almost_empty1 (almost_empty1est),
           .almost_full1 (almost_full1est)
       );

    //fifo_est estructural(/*AUTOINST*/
			/* // Outputs
			 .almost_empty_est	(almost_empty_est),
			 .errorEmpty		(errorEmpty),
			 .errorFull		(errorFull),
			 .almost_full		(almost_full),
			 .read_data_est		(read_data_est[7:0]),
			 // Inputs
			 .clk			(clk),
			 .empty_TH		(empty_TH[7:0]),
			 .full_TH		(full_TH[7:0]),
			 .read			(read),
			 .reset_L		(reset_L),
			 .write			(write),
			 .write_data		(write_data[7:0]));
*/

endmodule //test_mem;
