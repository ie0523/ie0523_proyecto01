module flipflop3b (
                    output reg [3:0] Q,
                    input [3:0] D,
                    input clk,
                    input reset_L);

  // Flip-flop de 4 bits
  always @(posedge clk) begin
  	if (!reset_L) begin
        Q <= 0;
    end else begin
        Q <= D;
    end
  end
endmodule
