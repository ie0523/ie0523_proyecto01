module memoria_cond (
    output reg [4:0] read_data_cond,  // Data OUT
    input [3:0] rd_dir,              // Direccion de lectura de memoria
    input [3:0] wr_dir,              // Direccion de escritura de memoria
    input [4:0] write_data,           // Data IN
    input memread,                    // POP
    input memwrite,                   // PUSH
    input clk,
    input reset_L);

reg [4:0] MEMORY [7:0];               // memoria de 8 espacios de 5 bits
integer i;                            // contador del for

always @(posedge clk) begin
    // Reiniciar memoria
    if (!reset_L) begin
        read_data_cond <= 0;
        // Recorre cada espacio de MEMORY
        for (i = 0; i < 8; i = i + 1) begin
            // Memoria en 0
            MEMORY[i] = 0;
        end
    end else begin
        if (memwrite) begin
            // Guarda en memoria el data IN
            MEMORY[wr_dir] <= write_data;
        end
        if (memread) begin
            // Obtiene el data de la memoria
            read_data_cond <= MEMORY[rd_dir];
        end else
        read_data_cond <= 0; // Valor por defecto
    end
end

endmodule
