module probador (
    input [4:0] read_data_cond,
    input [4:0] read_data_est,
    input almost_empty,
    input almost_full,
    input errorFull,
    input errorEmpty,
    output reg [4:0] write_data,
    output reg read,
    output reg write,
    output reg [3:0] empty_TH,
    output reg [3:0] full_TH,
    output reg clk,
    output reg reset_L);

    integer i, j;
    initial begin

        $dumpfile("fifo.vcd");
        $dumpvars;
        empty_TH <= 3;
        full_TH <= 6;
        write_data <= 0;
        write <= 0;
        read <= 0;
        reset_L <= 0;
        @(posedge clk)
            reset_L <= 0;
        @(posedge clk)
            reset_L <= 1;
        @(posedge clk) begin
            write_data <= $urandom%31;
            write <= 1;
        end

        for (i = 0; i < 12; i = i+1) begin
            @(posedge clk) begin
                write_data <= $urandom%31;
            end
        end
            write <= 0;
        @(posedge clk)
            read <= 1;
        repeat(10) begin
        @(posedge clk);
        end
        @(posedge clk)
            read <= 0;
        @(posedge clk) begin
            write <= 1;
        end
        @(posedge clk) begin
            write_data <= $urandom%31;
        end
        @(posedge clk) begin
            write_data <= $urandom%31;
        end
            read <= 1;
        for (i = 0; i < 15; i = i+1) begin
            @(posedge clk) begin
                write_data <= $urandom%31;
            end
        end

        $finish;
    end

    initial clk <= 0;
    always #1 clk <= ~clk;

endmodule
