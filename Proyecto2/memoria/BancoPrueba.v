`include "memoria_cond.v"
`include "probador.v"
`include "cmos_cells.v"
`include "memoria_est.v"

module BancoPrueba;
    /*AUTOWIRE*/
    // Beginning of automatic wires (for undeclared instantiated-module outputs)
    wire		clk;			// From pb of probador.v
    wire		read;		// From pb of probador.v
    wire		write;		// From pb of probador.v
    wire [3:0]		rd_dir;			// From pb of probador.v
    wire [4:0]		read_data_cond;		// From conductual of memoria_cond.v
    wire [4:0]		read_data_est;		// From estructural of memoria_est.v
    wire		reset_L;		// From pb of probador.v
    wire [3:0]		wr_dir;			// From pb of probador.v
    wire [4:0]		write_data;		// From pb of probador.v
    // End of automatics

    memoria_cond conductual(/*AUTOINST*/
			    // Outputs
			    .read_data_cond	(read_data_cond[4:0]),
			    // Inputs
			    .rd_dir		(rd_dir[3:0]),
			    .wr_dir		(wr_dir[3:0]),
			    .write_data		(write_data[4:0]),
			    .memread		(read),
			    .memwrite		(write),
			    .clk		(clk),
			    .reset_L		(reset_L));

    memoria_est estructural(/*AUTOINST*/
			    // Outputs
			    .read_data_est	(read_data_est[4:0]),
			    // Inputs
			    .clk		(clk),
			    .memread		(read),
			    .memwrite		(write),
			    .rd_dir		(rd_dir[3:0]),
			    .reset_L		(reset_L),
			    .wr_dir		(wr_dir[3:0]),
			    .write_data		(write_data[4:0]));

    probador pb(/*AUTOINST*/
		// Outputs
		.rd_dir			(rd_dir[3:0]),
		.wr_dir			(wr_dir[3:0]),
		.write_data		(write_data[4:0]),
		.memread		(read),
		.memwrite		(write),
		.clk			(clk),
		.reset_L		(reset_L),
		// Inputs
		.read_data_cond		(read_data_cond[4:0]),
		.read_data_est		(read_data_est[4:0]));
endmodule //test_mem;
