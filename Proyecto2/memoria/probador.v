module probador (
    output reg [3:0] rd_dir,
    output reg [3:0] wr_dir,
    output reg [4:0] write_data,
    output reg memread,
    output reg memwrite,
    output reg clk,
    output reg reset_L,
    input [4:0] read_data_cond,
    input [4:0] read_data_est
    );

    integer i, j, k, l;
    initial begin
        $dumpfile("mem.vcd");
        $dumpvars;

        memwrite <= 0;
        memread <= 0;
        reset_L <= 0;
        rd_dir <= 0;
        wr_dir <= 0;
        write_data <=0;

        @(posedge clk)
          reset_L <= 1;

        for (i = 0; i < 9; i = i+1) begin
            @(posedge clk) begin
                memwrite <= 1;
                wr_dir <= i;
                write_data <= $urandom%31;
            end
        end

        write_data <= 0;
        memwrite <= 0;

        for (j = 0; j < 8; j = j+1) begin
            @(posedge clk) begin
                rd_dir <= j;
                memread <= 1;
            end
        end

        @(posedge clk);
        memread <= 0;
        @(posedge clk)
         memwrite <= 1;
         write_data <= $urandom%31;
         rd_dir <= 0;
         wr_dir <= 0;

        for (k = 0; k < 8; k = k+1) begin
            @(posedge clk) begin
                memread <= 1;
                wr_dir <= k+1;
                rd_dir <= k;
                write_data <= $urandom%31;
            end
        end
        memwrite <= 0;
        write_data <= 0;
        memread <= 1;
        @(posedge clk);
        memread <= 0;
        @(posedge clk);

        #20
        $finish;
    end

    initial clk <= 0;
    always #10 clk <= ~clk;

endmodule
