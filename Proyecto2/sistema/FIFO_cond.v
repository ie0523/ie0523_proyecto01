module fifo_cond #(
parameter		BW=10,	// Byte/data width
parameter [3:0]	LEN=8)

(

	input	wire			i_clk, i_reset,
	input	wire			i_wr,
	input	wire [(BW-1):0]	i_data,
	input	wire			i_rd,
	output	reg [(BW-1):0]	o_data_cond,
	output  reg 			o_error_cond,
	output   				o_full_cond,
	output   				o_empty_cond,
	output   				o_almost_full_cond,
	output   				o_almost_empty_cond);

	reg	[(LEN-1):0] rdaddr, wraddr, o_fill;
	reg	[(BW-1):0]	mem	[0:(LEN-1)];
	reg overrun, underrun;
	wire full, empty;
	wire [(LEN-1):0] nxtaddr;

// always @(posedge i_clk)
// 	if (i_reset)
// 	begin
// 		rdaddr <= 0;
// 		wraddr <= 0;
// 		overrun  <= 0;
// 		underrun <= 0;
// 		o_fill   <= 0;
// 	end else if (i_wr)
// 	begin
// 		if (wraddr + 1'b1 != rdaddr)
// 		begin
// 			mem[wraddr] <= i_data;
// 			wraddr <= (wraddr + 1'b1);
// 			o_fill   <= o_fill + 1'b1;;
// 		end else
// 			overrun <= 1'b1;
// 	end else if (i_rd)
// 	begin
// 		if (wraddr != rdaddr) // If not empty
// 		begin
// 			o_data_cond <= mem[rdaddr];
// 			rdaddr <= rdaddr + 1'b1;
// 			o_fill   <= o_fill - 1'b1;;
// 		end else
// 			underrun <= 1'b1;
// 	end


	always @(posedge i_clk)
		if (i_wr)
			mem[wraddr] <= i_data;


	always @(*) begin
		o_data_cond = 0;
		if (i_rd) begin
			o_data_cond = mem[rdaddr];
		end
	end



	always @(posedge i_clk)
		if (~i_reset)
		begin
			wraddr <= 0;
			overrun  <= 0;
		end else if (i_wr)
		begin
			// Update the FIFO write address any time a write is made to
			// the FIFO and it's not FULL.
			//
			// OR any time a write is made to the FIFO at the same time a
			// read is made from the FIFO.
			if ((!full)||(i_rd)) begin
				if (wraddr == LEN-1) begin
					wraddr <=0;
				end else begin
					wraddr <= (wraddr + 1'b1);
				end
				overrun <= 0;
			end
			else
				overrun <= 1'b1;
		end


	always @(posedge i_clk)
		if (~i_reset)
		begin
			rdaddr <= 0;
			underrun <= 0;
		end else if (i_rd)
		begin
			// On any read request, increment the pointer if the FIFO isn't
			// empty--independent of whether a write operation is taking
			// place at the same time.
			if (!empty) begin
				if (rdaddr == LEN-1)begin
					rdaddr <= 0;
				end else begin
					rdaddr <= rdaddr + 1'b1;
				end
				underrun <= 0;
			end
			else
				// If a read is requested, but the FIFO was empty, set
				// an underrun error flag.
				underrun <= 1'b1;
		end


	always @(posedge i_clk) begin
		if (~i_reset)
		begin
			o_fill <= 0;
		end else casez({ i_wr, i_rd, !full, !empty })
		4'b01?1: o_fill <= o_fill - 1'b1;	// A successful read
		4'b101?: o_fill <= o_fill + 1'b1;	// A successful write
		4'b1110: o_fill <= o_fill + 1'b1;	// Successful write, failed read
		// 4'b11?1: Successful read *and* write -- no change
		default: o_fill <= o_fill;	// Default, no change
		endcase
	end
	// Porque esto no se puede poner en un assign is beyond me
	always @(*)
		o_error_cond = underrun | overrun;



	assign	nxtaddr = wraddr + 1'b1;
	assign	full  = (o_fill == LEN);
	assign	empty = (o_fill == 0);
	assign  o_full_cond = full;
	assign  o_empty_cond = empty;
	assign 	o_almost_empty_cond = (o_fill == 1);
	assign 	o_almost_full_cond = (LEN - o_fill <= 1);


endmodule
