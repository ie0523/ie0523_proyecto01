module controlador (
    input clk,
    input reset,
    input f6x8_0,
    input f6x8_1,
    input af4x8_0,
    input af4x8_1,
    input empty8x10_0,
    input empty8x10_1,
    input empty6x8_0,
    input empty6x8_1,
    input dest,


    output reg pop8x10_0,
    output reg pop8x10_1,
    output reg pop6x8_0,
    output reg pop6x8_1,
    output reg push6x8

);

reg tpush6x8;


always @(*) begin
pop8x10_0 = 0;
pop8x10_1 = 0;
pop6x8_0 = 0;
pop6x8_1 = 0;
tpush6x8 = 0;


    if (!dest) begin
      if (!f6x8_0 && !empty8x10_0) begin
        pop8x10_0=1;
        tpush6x8=1;
      end

      if (f6x8_0 && !empty8x10_0) begin
        pop8x10_0=0;
        tpush6x8=0;
      end

      if (!f6x8_0 && empty8x10_0 && empty8x10_1) begin
        pop8x10_1=0;
        tpush6x8=0;
      end

      if (!f6x8_0 && empty8x10_0 && !empty8x10_1) begin
        pop8x10_1=1;
        tpush6x8=1;
      end

      if (f6x8_0 && empty8x10_0 && empty8x10_1) begin
        pop8x10_1=0;
        tpush6x8=0;
      end

      if (f6x8_0 && empty8x10_0 && !empty8x10_1) begin
        pop8x10_1=0;
        tpush6x8=0;
      end
    end

    else begin
      if (!f6x8_1 && !empty8x10_0) begin
        pop8x10_0=1;
        tpush6x8=1;
      end

      if (f6x8_1 && !empty8x10_0) begin
        pop8x10_0=0;
        tpush6x8=0;
      end

      if (!f6x8_1 && empty8x10_0 && empty8x10_1) begin
        pop8x10_1=0;
        tpush6x8=0;
      end

      if (!f6x8_1 && empty8x10_0 && !empty8x10_1) begin
        pop8x10_1=1;
        tpush6x8=1;
      end

      if (f6x8_1 && empty8x10_0 && empty8x10_1) begin
        pop8x10_1=0;
        tpush6x8=0;
      end

      if (f6x8_1 && empty8x10_0 && !empty8x10_1) begin
        pop8x10_1=0;
        tpush6x8=0;
      end
    end




    if (!empty6x8_0 && !af4x8_0) begin
      pop6x8_0=1;
    end
    else begin
      pop6x8_0=0;
    end

    if (!empty6x8_1 && !af4x8_1) begin
      pop6x8_1=1;
    end
    else begin
      pop6x8_1=0;
    end

  end

always @ (posedge clk ) begin
      if (!reset) begin
        push6x8<=0;
      end
      else begin
        push6x8<=tpush6x8;
      end

end




endmodule
