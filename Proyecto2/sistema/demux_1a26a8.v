module demux_conductual6a8 (
    input clk,
    input [7:0] data_in,
    input selector,
    input reset,
    input push,
    output reg [7:0] data_out0,
    output reg [7:0] data_out1,
    output reg push0,
    output reg push1
);


    always @(*) begin
      data_out1 = 0;
      data_out0 = 0;
      push0=0;
      push1=0;

      if (reset) begin
        if(selector==0) begin
            data_out0=data_in;
            push0=push;
        end

        else begin
            data_out1=data_in;
            push1=push;
        end
      end

  end



endmodule
