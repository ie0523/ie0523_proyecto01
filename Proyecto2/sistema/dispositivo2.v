`include "serializacion_disp2.v"


module dispositivo2 (
                  input		         clk_4f,
	              input		         clk_32f,
                  input              in_serial_1,
                  input              in_serial_2,
                  output             validout_1,
                  output             validout_2,
                  input              reset,
                  output             active0,
                  output             active1,
                  output [7:0] dataout0,
                  output [7:0] dataout1,
                  output full0,
                  output empty0,
                  output full1,
                  output empty1,
                  output almost_empty0,
                  output almost_full0,
                  output almost_empty1,
                  output almost_full1,
                  input read
);

wire  [7:0]out_8bit_1;
wire  [7:0]out_8bit_2;


serializacion_disp2 serialdisp2(
                    .clk_4f         (clk_4f),
                    .clk_32f        (clk_32f),
                    .in_serial_1    (in_serial_1),
                    .in_serial_2    (in_serial_2),
                    .validout_1     (validout_1),
                    .validout_2     (validout_2),
                    .reset          (reset),
                    .active0         (active0),
                    .active1         (active1),
                    .out_8bit_1     (out_8bit_1),
                    .out_8bit_2     (out_8bit_2)
);


fifo_cond #(.BW(8), .LEN(4)) fifo4
(                 .o_data_cond (dataout0),
                  .o_error_cond (error0),
                  .o_full_cond     (full0),
                  .o_empty_cond    (empty0),
                  .o_almost_full_cond   (almost_full0),
                  .o_almost_empty_cond   (almost_empty0),
                  // Input
                  .i_wr   (validout_1),
                  .i_rd       (read),
                  .i_data (out_8bit_1),
                  .i_clk        (clk_4f),
                  .i_reset   (reset)

);


fifo_cond #(.BW(8), .LEN(4)) fifo5
(                 .o_data_cond (dataout1),
                  .o_error_cond (error1),
                  .o_full_cond     (full1),
                  .o_empty_cond    (empty1),
                  .o_almost_full_cond   (almost_full1),
                  .o_almost_empty_cond   (almost_empty1),
                  // Input
                  .i_wr   (validout_2),
                  .i_rd       (read),
                  .i_data (out_8bit_2),
                  .i_clk        (clk_4f),
                  .i_reset   (reset)

);


endmodule
