`include "Mux2a1.v"
`include "demux_1a26a8.v"
`include "FIFO_cond.v"

module enrutamiento (
    input [9:0] data_in0,
    input [9:0] data_in1,
    output [7:0] data_out0,
    output [7:0] data_out1,
    input write,
    input read0,
    input read1,
    input valid0,
    input valid1,
    input clk,
    input reset,
    input selectorautomatico,
    output full0,
    output empty0,
    output almost_empty0,
    output almost_full0,
    output full1,
    output empty1,
    output almost_empty1,
    output almost_full1,
    output destMux
);

wire [9:0] data_outmux;
wire [7:0] data_indemux;
wire destMux;
assign data_indemux = data_outmux [7:0];
assign destMux = data_outmux[8];
wire [7:0] salidaDemux0;
wire [7:0] salidaDemux1;


Mux2a1 mux1(
      .clk_f (clk),
			.data_inm0 (data_in0),
			.data_inm1 (data_in1),
			.data_outm (data_outmux),
			.valid0  (valid0),
			.valid1  (valid1),
      .selectorautomatico  (selectorautomatico),
      .reset  (reset),
			.valid_out  (valid_out)
);

demux_conductual6a8 demux_1 (
      .clk  (clk),
      .data_in (data_indemux),
      .selector (destMux),
      .reset  (reset),
      .push  (write),
      .data_out0 (salidaDemux0),
      .data_out1 (salidaDemux1),
      .push0 (push0),
      .push1 (push1)
);




fifo_cond #(.BW(8), .LEN(6)) fifo0
(                 .o_data_cond (data_out0),
                  .o_error_cond (error0),
                  .o_full_cond     (full0),
                  .o_empty_cond    (empty0),
                  .o_almost_full_cond   (almost_full0),
                  .o_almost_empty_cond   (almost_empty0),
                  // Input
                  .i_wr   (push0),
                  .i_rd       (read0),
                  .i_data (salidaDemux0),
                  .i_clk        (clk),
                  .i_reset   (reset)

);


fifo_cond #(.BW(8), .LEN(6)) fifo1
(                 .o_data_cond (data_out1),
                  .o_error_cond (error1),
                  .o_full_cond     (full1),
                  .o_empty_cond    (empty1),
                  .o_almost_full_cond   (almost_full1),
                  .o_almost_empty_cond   (almost_empty1),
                  // Input
                  .i_wr   (push1),
                  .i_rd       (read1),
                  .i_data (salidaDemux1),
                  .i_clk        (clk),
                  .i_reset   (reset)

);





endmodule
