`include "enrutamiento.v"
`include "trafico.v"
`include "serializacion.v"
`include "flop.v"

module interconexion (
    input [7:0] data_in,
    input class,
    input dest,
    input write,
    input read,
    input reset_L,
    input clk,
    input clk_32f,
    input pop8x10_0,
    input pop8x10_1,
    input pop6x8_0,
    input pop6x8_1,
    input push6x8,

    output destMux,
    output af6x8_0,
    output af6x8_1,
    output  empty8x10_0,
    output  empty8x10_1,
    output  empty6x8_0,
    output empty6x8_1,
    output 	            data_out0,
   	output 	            data_out1
);

wire [9:0] data_outtraf0;
wire [9:0] data_outtraf1;

wire [7:0] data_outenr0;
wire [7:0] data_outenr1;

wire           data_out_ps1;
wire           data_out_ps2;


trafico traf1(
    .data_in   (data_in),
    .class     (class),
    .dest      (dest),
    .data_out0   (data_outtraf0),
    .write       (write),
    .read0        (pop8x10_0),
    .read1        (pop8x10_1),
    .reset_L     (reset_L),
    .clk          (clk),
    .full0   (full0),
    .empty0  (empty8x10_0),
    .almost_empty0 (almost_empty0),
    .almost_full0 (almost_full0),
    .full1  (full0),
    .empty1  (empty8x10_1),
    .almost_empty1 (almost_empty1),
    .almost_full1 (almost_full1),
    .data_out1  (data_outtraf1)

);


enrutamiento enr1(
    .data_in0   (data_outtraf0),
    .data_in1   (data_outtraf1),
    .data_out0  (data_outenr0),
    .data_out1  (data_outenr1),
    .write      (push6x8),
    .read0       (pop6x8_0),
    .read1       (pop6x8_1),
    .clk   (clk),
    .reset (reset_L),
    .valid0 (pop8x10_0),
    .valid1 (pop8x10_1),
    .selectorautomatico (empty8x10_0),
    .full0    (f6x8_0),
    .empty0 (empty6x8_0),
    .almost_empty0 (almost_empty0en),
    .almost_full0 (af6x8_0),
    .full1 (f6x8_1),
    .empty1 (empty6x8_1),
    .almost_empty1 (almost_empty1en),
    .almost_full1  (af6x8_1),
    .destMux (destMux)
);

serializacion serial1 (
    .clk_4f (clk),
	.clk_32f (clk_32f),
	.in_paralela1 (data_outenr0),
	.in_paralela2 (data_outenr1),
	.valid1  (pop6x8_0),
	.valid2 (pop6x8_1),
	.data_out_ps1 (data_out_ps1),
	.data_out_ps2 (data_out_ps2),
	.reset (reset_L)
);


flop2mod flopacomodar( .clk  (clk_32f),
           .reset (reset_L),
           .datainflop (data_out_ps1),
           .dataoutflop (data_out0)
);


flop2mod fff( .clk  (clk_32f),
           .reset (reset_L),
           .datainflop (data_out_ps2),
           .dataoutflop (data_out1)
);
endmodule
