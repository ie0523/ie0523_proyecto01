module probadorsistema (
    input  [7:0] data_out0,
    input  [7:0] data_out_1,
    output reg [7:0] data_in,
    output reg read,
    output reg write,
    output reg class,
    output reg dest,
    output reg clk,
    output reg clk_32f,
    output reg reset_L);

    integer i, j;
    initial begin

        $dumpfile("sistema.vcd");
        $dumpvars;
        data_in <= 0;
        write <= 0;
        read <= 0;
        reset_L <= 0;
        dest<=0;
        class<=0;

         @(posedge clk)
            reset_L <= 0;
        @(posedge clk)
            reset_L <= 1;
        @(posedge clk)
            reset_L <= 1;
        @(posedge clk)
            reset_L <= 1;

        @(posedge clk) begin
            data_in <= $urandom%31;
            dest <= 0;
            class <= 1;
            write <= 1;
        end

        for (i = 0; i < 34; i = i+1) begin
            @(posedge clk) begin
                data_in <= $urandom%31;
                dest <= ~dest;
               class <= ~class;
            end
        end

        for (i = 0; i < 20; i = i+1) begin
            @(posedge clk) begin
                read <= 1;
                write <= 0;
            end
        end

        @(posedge clk)
            read <= 0;
        @(posedge clk)
            read <= 0;








        $finish;
    end

    initial clk <= 0;
    always #64 clk <= ~clk;

    initial clk_32f <= 1;
    always #8 clk_32f <= ~clk_32f;

endmodule
