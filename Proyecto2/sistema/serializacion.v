`include "paralelo_serial.v"

module serializacion (
    input		         clk_4f,
	input		         clk_32f,
	input [0:7]          in_paralela1,
	input [0:7]          in_paralela2,
	input		         valid1,
	input		         valid2,
	output 	             data_out_ps1,
	output 	             data_out_ps2,
	input 		         reset
);
paralelo_serial ps1_(.clk_4f (clk_4f),
                    .clk_32f (clk_32f),
                    .data_in_paralelo_8bit (in_paralela1),
                    .data_out_serial_1bit (data_out_ps1),
                    .reset (reset),
                    .valid (valid1)
);

paralelo_serial ps2_(.clk_4f (clk_4f),
                    .clk_32f (clk_32f),
                    .data_in_paralelo_8bit (in_paralela2),
                    .data_out_serial_1bit  (data_out_ps2),
                    .reset (reset),
                    .valid (valid2)
);

endmodule
