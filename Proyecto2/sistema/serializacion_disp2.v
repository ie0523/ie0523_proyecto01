`include "serial_paralelo.v"

module serializacion_disp2 (
    input		         clk_4f,
	input		         clk_32f,
    input                in_serial_1,
    input                in_serial_2,
    output               validout_1,
    output               validout_2,
    input                reset,
    output               active0,
    output               active1,
	output   [0:7]	     out_8bit_1,
	output  [0:7]	     out_8bit_2

);


serial_paralelo sp1_(.clk_4f (clk_4f),
                     .clk_32f (clk_32f),
                     .data_in_1bit (in_serial_1),
                     .reset (reset),
                     .validout (validout_1),
                     .active (active0),
                     .data_out_8bit (out_8bit_1)
);


serial_paralelo sp2_(.clk_4f (clk_4f),
                     .clk_32f (clk_32f),
                     .data_in_1bit (in_serial_2),
                     .reset (reset),
                     .validout (validout_2),
                     .active (active1),
                     .data_out_8bit (out_8bit_2)
);

endmodule
