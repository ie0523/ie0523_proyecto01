`include "interconexion.v"
`include "dispositivo2.v"
`include "controlador.v"

module sistema(
    input [7:0] data_in,
    input class,
    input dest,
    input write,
    input read,
    input reset_L,
    input clk,
    input clk_32f,
    output 	  [7:0]          data_out0,
  	output 	    [7:0]         data_out1
);

  wire a2,a3;

  interconexion interc(
    .data_in                  (data_in),
    .class                   (class),
    .dest                    (dest),
    .write                   (write),
    .read                    (read),
    .reset_L                  (reset_L),
    .pop8x10_0                (pop8x10_0),
    .pop8x10_1                (pop8x10_1),
    .pop6x8_0                  (pop6x8_0),
    .push6x8                  (push6x8),
    .pop6x8_1                   (pop6x8_1),
    .af6x8_0                     (af6x8_0 ),
    .af6x8_1                     (af6x8_1),
    .empty8x10_0                 (empty8x10_0),
    .empty8x10_1                 (empty8x10_1 ),
    .empty6x8_0                  (empty6x8_0),
    .empty6x8_1                   (empty6x8_1),
    .clk                      (clk),
    .clk_32f                  (clk_32f),
    .data_out0                (a2),
	  .data_out1                 (a3),
    .destMux                  (destMux)
);


      dispositivo2 disposi2 (
                  .clk_4f  (clk),
	               .clk_32f (clk_32f),
                  .in_serial_1 (a2),
                  .in_serial_2 (a3),
                  .validout_1 (validout_1),
                  .validout_2 (validout_2),
                  .reset (reset_L),
                  .active0 (active0),
                  .active1 (active1),
	               .dataout0 (data_out0),
	               .dataout1 (data_out1),
                  .full0 (full0),
                  .empty0 (empty0),
                  .almost_empty0 (almost_empty0),
                  .almost_full0 (af4x8_0),
                  .full1 (full1),
                  .empty1 (empty1),
                  .almost_empty1 (almost_empty1),
                  .almost_full1 (af4x8_1),
                  .read  (read)
                  );

controlador c1(
      .clk (clk),
      .reset (reset_L),
      .f6x8_0  (af6x8_0),
      .f6x8_1  (af6x8_1),
      .af4x8_0  (af4x8_0),
      .af4x8_1   (af4x8_1),
      .empty8x10_0 (empty8x10_0),
      .empty8x10_1 (empty8x10_1),
      .empty6x8_0  (empty6x8_0),
      .empty6x8_1  (empty6x8_1),
      .dest        (destMux),
      .pop8x10_0   (pop8x10_0),
      .pop8x10_1    (pop8x10_1),
      .pop6x8_0    (pop6x8_0),
      .pop6x8_1    (pop6x8_1),
      .push6x8     (push6x8)

  );




endmodule
