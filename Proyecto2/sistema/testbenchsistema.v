`include "probadorsistema.v"
`include "cmos_cells.v"
`include "sistema.v"
`include "sistema_synth.v"

module BancoPrueba;

    wire		clk, clk_32f;
	wire [7:0]  data_in;

    wire  [7:0]	data_out0;
    wire  [7:0]		data_out1;
    wire  [7:0]	data_out0est;
    wire  [7:0]		data_out1est;
    wire		read;
    wire		reset_L;
    wire		write;
	wire class;
    wire dest;
	wire full;
    wire empty;
    wire almost_empty;
    wire almost_full;
	wire fullest;
    wire emptyest;
    wire almost_emptyest;
    wire almost_fullest;

   sistema sistemcond (
	        .data_in			(data_in),
			.class  (class),
			.dest   (dest),
    		.reset_L         (reset_L),
    		.clk            (clk),
    		.clk_32f        (clk_32f),
   	       .data_out0    (data_out0),
	        .data_out1    (data_out1),
			.write (write),
			.read (read)
			);
   sistema_synt sistemsintetiz (
	        .data_in			(data_in),
			.class  (class),
			.dest   (dest),
    		.reset_L         (reset_L),
    		.clk            (clk),
    		.clk_32f        (clk_32f),
   	        .data_out0    (data_out0est),
	        .data_out1    (data_out1est),
			.write (write),
			.read (read)
			);

   probadorsistema pb(/*AUTOINST*/
		      // Outputs
		      .data_in		(data_in),
		      .read		(read),
		      .write		(write),
			  .class  (class),
			  .dest   (dest),
    		.clk            (clk),
    		.clk_32f        (clk_32f),
    		.reset_L         (reset_L),
			 // Inputs
		      .data_out0	(data_out0),
		      .data_out_1	(data_out1)

    );

endmodule
