`include "demux_1a28a10.v"

module trafico (
    input [7:0] data_in,
    input class,
    input dest,
    output [9:0] data_out0,
    input write,
    input read0,
    input read1,
    input reset_L,
    input clk,
    output full0,
    output empty0,
    output almost_empty0,
    output almost_full0,
    output full1,
    output empty1,
    output almost_empty1,
    output almost_full1,
    output [9:0] data_out1
);
wire  [9:0] data_in10b;
assign data_in10b={class,dest,data_in};
wire [9:0] a1;
wire [9:0] a2;
wire push0;
wire push1;
wire error0;
wire error1;

demux_conductual8a10 demux_1 (
      .clk  (clk),
      .data_in (data_in10b),
      .selector (class),
      .reset  (reset_L),
      .push   (write),
      .data_out0 (a1),
      .data_out1 (a2),
      .push0 (push0),
      .push1 (push1)
);




fifo_cond fifo1
(                 .o_data_cond (data_out0),
                  .o_error_cond (error0),
                  .o_full_cond     (full0),
                  .o_empty_cond    (empty0),
                  .o_almost_full_cond   (almost_full0),
                  .o_almost_empty_cond   (almost_empty0),
                  // Input
                  .i_wr   (push0),
                  .i_rd       (read0),
                  .i_data (a1),
                  .i_clk        (clk),
                  .i_reset   (reset_L)

);

fifo_cond fifo2
(                 .o_data_cond (data_out1),
                  .o_error_cond (error1),
                  .o_full_cond     (full1),
                  .o_empty_cond    (empty1),
                  .o_almost_full_cond   (almost_full1),
                  .o_almost_empty_cond   (almost_empty1),
                  // Input
                  .i_wr   (push1),
                  .i_rd       (read1),
                  .i_data (a2),
                  .i_clk        (clk),
                  .i_reset   (reset_L)


);

endmodule
