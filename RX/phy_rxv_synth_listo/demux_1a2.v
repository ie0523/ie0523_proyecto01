module demux_conductual (
    input clk,
    input [7:0] data_in,
    input validin,
    input selector,
    input reset, 
    output reg valid_out0,
    output reg valid_out1,
    output reg [7:0] data_out0,
    output reg [7:0] data_out1
);
reg [7:0] aux0; //quitar esto
reg [7:0] aux1;

  always @ ( posedge clk ) begin
    if (reset==0) begin
     aux1 <= 0;
     aux0 <= 0;
    end 
    else
      aux1 <= data_out1;
      aux0 <= data_out0;
      end


    always @(*) begin
    data_out0=aux0;
    data_out1=aux1;
    valid_out0=0;
    valid_out1=0; 
    if (reset==0) begin 

    data_out0=4'b0000;
    data_out1=4'h0;
    valid_out0=0;
    valid_out1=0;

    end else if(selector==0 & validin==1) begin 

        data_out0=data_in;
        valid_out0=validin;

    end else if(selector==1 & validin==1) begin 

        data_out1=data_in;
        valid_out1=validin;

    end else begin
        valid_out0=0;
        valid_out1=0;
    end

end
endmodule













  //   end else if (validin == 0) begin
  //     valid_out0=0;
  //     valid_out1=0;
  //   end

  //   else begin
  //     if (selector==0) begin
  //       data_out0 = data_in;
  //       valid_out0=validin;
  //     end
  //     if (selector==1) begin
  //       data_out1=data_in;
  //       valid_out1=validin;
  //     end

  //   end

  // end



