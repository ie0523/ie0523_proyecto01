module etapa_flops_4 (
    input clk,
    input [7:0] data0,
    input [7:0] data1,
    input [7:0] data2,
    input [7:0] data3,
    input reset,
    input valid0,
    input valid1,
    input valid2,
    input valid3,
    output reg valid_out0,
    output reg valid_out1,
    output reg valid_out2,
    output reg valid_out3, 
    output reg [7:0] out0,
    output reg [7:0] out1,
    output reg [7:0] out2,
    output reg [7:0] out3
);

always @(posedge clk)begin
    if (reset==0) begin
    out0=0;
    out1=0;
    out2=0;
    out3=0;
    valid_out0=0;
    valid_out1=0;
    valid_out2=0;
    valid_out3=0;
    end else begin 
    valid_out0=valid0;
    valid_out1=valid1;
    valid_out2=valid2;
    valid_out3=valid3;
    out0=data0;
    out1=data1;
    out2=data2;
    out3=data3;
    end
end

endmodule
