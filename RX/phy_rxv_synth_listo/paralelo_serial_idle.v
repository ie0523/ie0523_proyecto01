module paralelo_serial (
    input		 clk_4f,
	input		 clk_32f,
	input 		 reset,
    input       active,
	output reg	 data_out_serial_1bit
);

reg [0:2] contador;
reg [0:7] aux;
reg [0:7] BC=8'hBC;
reg [0:7] IDL=8'h7C;

always @(*) begin
    if (active==0 && reset==1) begin
     aux=BC;
    end
    else begin
        aux= IDL;
    end
end

always @(posedge clk_32f)begin 
    if (reset==0) begin 
        contador<=0;
        data_out_serial_1bit<=0; 
    end 
    else begin 
    data_out_serial_1bit<= aux[contador];
    contador <=contador+1;
    end
end
/***always @(*) begin
    if (valid==0 && reset==1) begin
     aux=BC;
    end
    else begin
        aux= data_in_paralelo_8bit;
    end
end
always @(posedge clk_32f)begin 
    if (reset==0) begin 
        contador<=0;
        data_out_serial_1bit<=0; 
    end 
    else begin 
    data_out_serial_1bit<= aux[contador];
    contador <=contador+1;
    end
end***/


endmodule
