`include "demux1.v"
`include "demux2.v"
`include "serial_paralelo.v"
`include "paralelo_serial_idle.v"
`include "etapa_flops2.v"
`include "etapa_flops4.v"

module phy_rx (
    input clk_f, 
    input clk_2f, 
    input clk_4f, 
    input clk_32f, 
    input out_ps_tx,
    output [0:7] data_out_0_rx,
    output [0:7] data_out_1_rx,
    output [0:7] data_out_2_rx,
    output [0:7] data_out_3_rx,
    output valid_out0_rx,
    output valid_out1_rx,
    input reset,
    output data_out_ps_idle,
    output valid_out2_rx,
    output valid_out3_rx
    );

wire validoutsp;
wire activesp;
wire [0:7]  data_out_8bitsp;
wire valid_out0_L2;
wire valid_out1_L2;
wire [0:7]  data_out_demux_L2_0_L2;
wire [0:7]  data_out_demux_L2_1_L2;
wire validout0_flop2_f2;
wire validout1_flop2_f2;
wire [0:7] out0_flop2_f2;
wire [0:7]  out1_flop2_f2;
wire valid_out0_L1;
wire valid_out1_L1;
wire [0:7]  data_out_demux_L1_0_L1;
wire [0:7]  data_out_demux_L1_1_L1;
wire [0:7]  data_out_demux_L1_2_L1;
wire [0:7]  data_out_demux_L1_3_L1;


    serial_paralelo caja1( .clk_4f  (clk_4f),
	        .clk_32f    (clk_32f),
	        .data_in_1bit   (out_ps_tx),
	        .reset  (reset),
            .validout   (validoutsp),
            .active (activesp),
            .data_out_8bit  (data_out_8bitsp)
);


    paralelo_serial caja2( .clk_4f  (clk_4f),
                    .clk_32f    (clk_32f),
                    .reset      (reset),
                    .active     (activesp),
                    .data_out_serial_1bit       (data_out_ps_idle)
    );

    demux2 caja3( .clk_f       (clk_4f),
                .data_in_demux_L2_0     (data_out_8bitsp),
                .validin        (validoutsp),
                .selector       (~clk_2f),
                .reset      (reset),
                .valid_out0     (valid_out0_L2),
                .valid_out1     (valid_out1_L2),
                .data_out_demux_L2_0    (data_out_demux_L2_0_L2),
                .data_out_demux_L2_1    (data_out_demux_L2_1_L2)
    );

    etapa_flops_2 caja4(    .clk        (clk_2f),
                        .data0_flop2        (data_out_demux_L2_0_L2),
                        .data1_flop2        (data_out_demux_L2_1_L2),
                        .valid0_flop2       (valid_out0_L2),
                        .valid1_flop2       (valid_out1_L2),
                        .validout0_flop2    (validout0_flop2_f2),
                        .validout1_flop2    (validout1_flop2_f2),
                        .reset      (reset), 
                        .out0_flop2     (out0_flop2_f2),
                        .out1_flop2     (out1_flop2_f2)
    );
   demux1 caja5(     .clk_f  (clk_2f),
                    .data_in_demux_L1_0 (out1_flop2_f2),
                    .data_in_demux_L1_1 (out0_flop2_f2),
                    .validin0    (validout1_flop2_f2),
                    .validin1    (validout1_flop2_f2),
                    .selector   (~clk_f),
                    .selector2 (~clk_f),
                    .valid_out0     (valid_out0_L1),
                    .valid_out1     (valid_out1_L1),
                    .valid_out2     (valid_out2_L1),
                    .valid_out3     (valid_out3_L1),
                    .reset (reset),
                    .data_out_demux_L1_0    (data_out_demux_L1_0_L1),
                    .data_out_demux_L1_1    (data_out_demux_L1_1_L1),
                    .data_out_demux_L1_2    (data_out_demux_L1_2_L1),
                    .data_out_demux_L1_3    (data_out_demux_L1_3_L1)
);
etapa_flops_4 caja6( .clk       (clk_f),
                    .data0      (data_out_demux_L1_0_L1),
                    .data1      (data_out_demux_L1_1_L1),
                    .data2      (data_out_demux_L1_2_L1),
                    .data3      (data_out_demux_L1_3_L1),
                    .reset      (reset),
                    .valid0     (valid_out0_L1),
                    .valid1     (valid_out1_L1),
                    .valid2     (valid_out2_L1),
                    .valid3     (valid_out3_L1),
                    .valid_out0     (valid_out0_rx),
                    .valid_out1     (valid_out1_rx),
                    .valid_out2     (valid_out2_rx),    
                    .valid_out3     (valid_out3_rx),
                    .out0       (data_out_0_rx),
                    .out1       (data_out_1_rx),
                    .out2       (data_out_2_rx),
                    .out3       (data_out_3_rx)
);

endmodule
