module probador_rx(    output reg clk_f, 
    output reg clk_2f, 
    output reg clk_4f, 
    output reg clk_32f, 
    output reg out_ps_tx,
    input [0:7] data_out_0_rx,
    input [0:7] data_out_1_rx,
    input [0:7] data_out_2_rx,
    input [0:7] data_out_3_rx,
    input valid_out0_rx,
    input valid_out1_rx,
    input valid_out2_rx,
    inout valid_out3_rx,
    input data_out_ps_idle,
    output reg reset
    );

initial begin
    $dumpfile("rx.vcd");
    $dumpvars;
	out_ps_tx<=0;//Se inicializan las entradas en 0
    reset<=0;

    @(posedge clk_32f);
        reset<=0;
    @(posedge clk_32f);
        reset<=0;
    @(posedge clk_32f);
        reset<=0;
    @(posedge clk_32f);
        reset<=0;
    @(posedge clk_32f);
        reset<=0;
    @(posedge clk_32f);
        reset<=0;
    @(posedge clk_32f);
        reset<=0;
    @(posedge clk_32f);
        reset<=1;
        out_ps_tx<=1;
///////////////////////////////



    @(posedge clk_32f);
        out_ps_tx<=0;

    @(posedge clk_32f);
        out_ps_tx<=1;
    @(posedge clk_32f);
        out_ps_tx<=1;
    @(posedge clk_32f);
        out_ps_tx<=1;
    @(posedge clk_32f);
        out_ps_tx<=1;
    @(posedge clk_32f);
        out_ps_tx<=0;
    @(posedge clk_32f);
        out_ps_tx<=0;


                ///////////////////////////////
                @(posedge clk_32f);
                    out_ps_tx<=1;

                @(posedge clk_32f);
                    out_ps_tx<=0;

                @(posedge clk_32f);
                    out_ps_tx<=1;
                @(posedge clk_32f);
                    out_ps_tx<=1;
                @(posedge clk_32f);
                    out_ps_tx<=1;
                @(posedge clk_32f);
                    out_ps_tx<=1;
                @(posedge clk_32f);
                    out_ps_tx<=0;
                @(posedge clk_32f);
                    out_ps_tx<=0;
                    ///////////////////////////////
                    @(posedge clk_32f);
                        out_ps_tx<=1;

                    @(posedge clk_32f);
                        out_ps_tx<=0;

                    @(posedge clk_32f);
                        out_ps_tx<=1;
                    @(posedge clk_32f);
                        out_ps_tx<=1;
                    @(posedge clk_32f);
                        out_ps_tx<=1;
                    @(posedge clk_32f);
                        out_ps_tx<=1;
                    @(posedge clk_32f);
                        out_ps_tx<=0;
                    @(posedge clk_32f);
                        out_ps_tx<=0;
                        ///////////////////////////////
                        @(posedge clk_32f);
                            out_ps_tx<=1;

                        @(posedge clk_32f);
                            out_ps_tx<=0;

                        @(posedge clk_32f);
                            out_ps_tx<=1;
                        @(posedge clk_32f);
                            out_ps_tx<=1;
                        @(posedge clk_32f);
                            out_ps_tx<=1;
                        @(posedge clk_32f);
                            out_ps_tx<=1;
                        @(posedge clk_32f);
                            out_ps_tx<=0;
                        @(posedge clk_32f);
                            out_ps_tx<=0;
        ////////////////////////////////
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                    out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;           
                @(posedge clk_32f); 
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;

        ////////////////////////////////
                @(posedge clk_32f);
                out_ps_tx<=0;
                @(posedge clk_32f);
                    out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=0;           
                @(posedge clk_32f); 
                out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;

        ////////////////////////////////
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                    out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;           
                @(posedge clk_32f); 
                out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=0;

        ////////////////////////////////
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                    out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;           
                @(posedge clk_32f); 
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;

        ////////////////////////////////
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                    out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;           
                @(posedge clk_32f); 
                out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=0;

        ////////////////////////////////
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                    out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;           
                @(posedge clk_32f); 
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;

        ////////////////////////////////
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                    out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=0;           
                @(posedge clk_32f); 
                out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=0;

        ////////////////////////////////
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                    out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=0;           
                @(posedge clk_32f); 
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=1;

        ////////////////////////////////
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                    out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;           
                @(posedge clk_32f); 
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=0;

        ////////////////////////////////
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                    out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=0;           
                @(posedge clk_32f); 
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=0;

        ////////////////////////////////
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                    out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=0;           
                @(posedge clk_32f); 
                out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=0;

        ////////////////////////////////
                @(posedge clk_32f);
                out_ps_tx<=0;
                @(posedge clk_32f);
                    out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=0;           
                @(posedge clk_32f); 
                out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=1;

        ////////////////////////////////
                @(posedge clk_32f);
                out_ps_tx<=0;
                @(posedge clk_32f);
                    out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=1;           
                @(posedge clk_32f); 
                out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=1;
        ////////////////////////////////
                @(posedge clk_32f);
                out_ps_tx<=0;
                @(posedge clk_32f);
                    out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=1;           
                @(posedge clk_32f); 
                out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=1;
        ////////////////////////////////
                @(posedge clk_32f);
                out_ps_tx<=1;
                @(posedge clk_32f);
                    out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=1;           
                @(posedge clk_32f); 
                out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=0;
                @(posedge clk_32f);
                out_ps_tx<=1;



    $finish;
end

initial clk_f <= 0;
always #128 clk_f <= ~clk_f;


initial clk_2f <= 1;
always #64 clk_2f <= ~clk_2f;

initial clk_4f <= 1;
always #32 clk_4f <= ~clk_4f;

initial clk_32f <= 1;
always #4 clk_32f <= ~clk_32f;


endmodule
