`include "phy_rx.v"
`include "probador_rx.v"
`include "phy_rx_synth.v"
`include "cmos_cells.v"
module testbench_rx;

wire clk_f;
wire clk_2f; 
wire clk_4f; 
wire clk_32f; 
wire out_ps_tx;
wire out_ps_tx2;
wire [0:7] data_out_0_rx;
wire [0:7] data_out_1_rx;
wire [0:7] data_out_2_rx;
wire [0:7] data_out_3_rx;
wire valid_out0_rx;
wire valid_out1_rx;
wire valid_out2_rx;
wire valid_out3_rx;
wire [0:7] data_out_0_rxest;
wire [0:7] data_out_1_rxest;
wire [0:7] data_out_2_rxest;
wire [0:7] data_out_3_rxest;
wire valid_out0_rxest;
wire valid_out1_rxest;
wire valid_out2_rxest;
wire data_out_ps_idle_est;
wire valid_out3_rxest;
wire reset;
wire selector;


phy_rx rx( /*AUTOINST*/
	  // Outputs
	  .data_out_0_rx		(data_out_0_rx[0:7]),
	  .data_out_1_rx		(data_out_1_rx[0:7]),
	  .data_out_2_rx		(data_out_2_rx[0:7]),
	  .data_out_3_rx		(data_out_3_rx[0:7]),
	  .valid_out0_rx		(valid_out0_rx),
	  .valid_out1_rx		(valid_out1_rx),
	  .data_out_ps_idle		(data_out_ps_idle),
	  .valid_out2_rx		(valid_out2_rx),
	  .valid_out3_rx		(valid_out3_rx),
	  // Inputs
	  .clk_f			(clk_f),
	  .clk_2f			(clk_2f),
	  .clk_4f			(clk_4f),
	  .clk_32f			(clk_32f),
	  .out_ps_tx			(out_ps_tx),
	  .reset			(reset));

phy_rx_synth rx_synth( /*AUTOINST*/
		      // Outputs
		      .data_out_0_rx	(data_out_0_rxest),
		      .data_out_1_rx	(data_out_1_rxest),
		      .data_out_2_rx	(data_out_2_rxest),
		      .data_out_3_rx	(data_out_3_rxest),
		      .data_out_ps_idle	(data_out_ps_idle_est),
		      .valid_out0_rx	(valid_out0_rxest),
		      .valid_out1_rx	(valid_out1_rxest),
		      .valid_out2_rx	(valid_out2_rxest),
		      .valid_out3_rx	(valid_out3_rxest),
		      // Inputs
		      .clk_2f		(clk_2f),
		      .clk_32f		(clk_32f),
		      .clk_4f		(clk_4f),
		      .clk_f		(clk_f),
		      .out_ps_tx	(out_ps_tx),
		      .reset		(reset));

probador_rx prx( /*AUTOINST*/
		// Outputs
		.clk_f			(clk_f),
		.clk_2f			(clk_2f),
		.clk_4f			(clk_4f),
		.clk_32f		(clk_32f),
		.out_ps_tx		(out_ps_tx),
		.reset			(reset),
		// Inouts
		.valid_out3_rx		(valid_out3_rx),
		// Inputs
		.data_out_0_rx		(data_out_0_rx[0:7]),
		.data_out_1_rx		(data_out_1_rx[0:7]),
		.data_out_2_rx		(data_out_2_rx[0:7]),
		.data_out_3_rx		(data_out_3_rx[0:7]),
		.valid_out0_rx		(valid_out0_rx),
		.valid_out1_rx		(valid_out1_rx),
		.valid_out2_rx		(valid_out2_rx),
		.data_out_ps_idle	(data_out_ps_idle));

endmodule
