module Recirculacion (
    input clk_f,
    input  selectorRe ,
    input [7:0] data_in0,
    input [7:0] data_in1,
    input [7:0] data_in2,
    input [7:0] data_in3,
    input valid0,
    input valid1,
    input valid2,
    input valid3,
    output reg [7:0] data_out0,
    output reg [7:0] data_out1,
    output reg [7:0] data_out2,
    output reg [7:0] data_out3,
    output reg [7:0] data_out4,
    output reg [7:0] data_out5,
    output reg [7:0] data_out6,
    output reg [7:0] data_out7
);

reg [7:0] out0;
reg [7:0] out1;
reg [7:0] out2;
reg [7:0] out3;
reg [7:0] out4;
reg [7:0] out5;
reg [7:0] out6;
reg [7:0] out7;


always @(posedge clk_f) begin
   data_out0<=out0;
   data_out1<=out1;
   data_out2<=out2;
   data_out3<=out3;
   data_out4<=out4;
   data_out5<=out5;
   data_out6<=out6;
   data_out7<=out7;
end

//0 No hay IDLE

always @(*) begin
    if (selectorRe == 1) begin
        out0=0;
        out1=0;
        out2=0;
        out3=0;
        out4=data_in0;
        out5=data_in1;
        out6=data_in2;
        out7=data_in3;
    end
    else  begin
       out0=data_in0;
       out1=data_in1;
       out2=data_in2;
       out3=data_in3;
       out4=0;
       out5=0;
       out6=0;
       out7=0;
    end

end

endmodule
