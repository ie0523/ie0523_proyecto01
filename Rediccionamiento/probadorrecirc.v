module probadorrecirc(
    output reg clk_f,
    output reg  selectorRe ,
    output reg  [7:0] data_in0,
    output reg  [7:0] data_in1,
    output reg  [7:0] data_in2,
    output reg  [7:0] data_in3,
    output reg  valid0,
    output reg valid1,
    output reg valid2,
    output reg valid3,
    output reg clk_f_estr,
    output reg selectorRe_estr,
    output reg [7:0] data_in0_estr,
    output reg [7:0] data_in1_estr,
    output reg [7:0] data_in2_estr,
    output reg [7:0] data_in3_estr,
    output reg valid0_estr,
    output reg valid1_estr,
    output reg valid2_estr,
    output reg valid3_estr,
    input [7:0] data_out0,
    input [7:0] data_out1,
    input [7:0] data_out2,
    input [7:0] data_out3,
    input [7:0] data_out4,
    input [7:0] data_out5,
    input [7:0] data_out6,
    input [7:0] data_out7,
    input [7:0] data_out0_estr,
    input [7:0] data_out1_estr,
    input [7:0] data_out2_estr,
    input [7:0] data_out3_estr,
    input [7:0] data_out4_estr,
    input [7:0] data_out5_estr,
    input [7:0] data_out6_estr,
    input [7:0] data_out7_estr
);

initial begin
   $dumpfile("Re.vcd");
   $dumpvars;
    data_in0<=0;
    data_in1<=0;
    data_in2<=0;
    data_in3<=0;
    valid0<=1;
    valid1<=1;
    valid2<=1;
    valid3<=1;
    data_in0_estr<=0;
    data_in1_estr<=0;
    data_in2_estr<=0;
    data_in3_estr<=0;
    valid0_estr<=1;
    valid1_estr<=1;
    valid2_estr<=1;
    valid3_estr<=1;
    @(posedge clk_f);
    data_in0<=8'hFF;
    data_in1<=8'hEE;
    data_in2<=8'hDD;
    data_in3<=8'hCC;
    data_in0_estr<=8'hFF;
    data_in1_estr<=8'hEE;
    data_in2_estr<=8'hDD;
    data_in3_estr<=8'hCC;
    @(posedge clk_f);
    data_in0<=8'hBB;
    data_in1<=8'hAA;
    data_in2<=8'h99;
    data_in3<=8'h88;
    data_in0_estr<=8'hBB;
    data_in1_estr<=8'hAA;
    data_in2_estr<=8'h99;
    data_in3_estr<=8'h88;

    @(posedge clk_f);
     data_in2<=8'h77;
     data_in2_estr<=8'h77;
    @(posedge clk_f);

    data_in0<=8'hFF;
    data_in1<=8'hEE;
    data_in2<=8'hDD;
    data_in3<=8'hCC;
    data_in0_estr<=8'hFF;
    data_in1_estr<=8'hEE;
    data_in2_estr<=8'hDD;
    data_in3_estr<=8'hCC;
    @(posedge clk_f);

    data_in0<=8'hBB;
    data_in1<=8'hAA;
    data_in2<=8'h99;
    data_in3<=8'h88;
    data_in0_estr<=8'hBB;
    data_in1_estr<=8'hAA;
    data_in2_estr<=8'h99;
    data_in3_estr<=8'h88;

    @(posedge clk_f);

     data_in2<=8'h77;
     data_in2_estr<=8'h77;
    @(posedge clk_f);

    data_in0<=8'hFF;
    data_in1<=8'hEE;
    data_in2<=8'hDD;
    data_in3<=8'hCC;
    data_in0_estr<=8'hFF;
    data_in1_estr<=8'hEE;
    data_in2_estr<=8'hDD;
    data_in3_estr<=8'hCC;
    @(posedge clk_f);

    data_in0<=8'hBB;
    data_in1<=8'hAA;
    data_in2<=8'h99;
    data_in3<=8'h88;

    data_in0_estr<=8'hBB;
    data_in1_estr<=8'hAA;
    data_in2_estr<=8'h99;
    data_in3_estr<=8'h88;


    @(posedge clk_f);
     data_in2<=8'h77;
     data_in2_estr<=8'h77;
     $finish;
end

initial clk_f <= 0;
always #40 clk_f <= ~clk_f;

initial selectorRe <= 0;
always #80 selectorRe <= ~selectorRe;

initial clk_f_estr <= 0;
always #40 clk_f_estr <= ~clk_f_estr;
endmodule
