`timescale 	1ns				/ 100ps
`include "probadorrecirc.v"
`include "Recirculacion.v"
`include "cmos_cells.v"
`include "synth_recirculacion.v"
module testbench;
wire clk_f;
wire selectorRe;
wire [7:0] data_in0;
wire [7:0] data_in1;
wire [7:0] data_in2;
wire [7:0] data_in3;
wire valid0;
wire valid1;
wire valid2;
wire valid3;
wire [7:0] data_out0;
wire [7:0] data_out1;
wire [7:0] data_out2;
wire [7:0] data_out3;
wire [7:0] data_out4;
wire [7:0] data_out5;
wire [7:0] data_out6;
wire [7:0] data_out7;
wire clk_f_estr;
wire selectorRe_estr;
wire [7:0] data_in0_estr;
wire [7:0] data_in1_estr;
wire [7:0] data_in2_estr;
wire [7:0] data_in3_estr;
wire valid0_estr;
wire valid1_estr;
wire valid2_estr;
wire valid3_estr;
wire [7:0] data_out0_estr;
wire [7:0] data_out1_estr;
wire [7:0] data_out2_estr;
wire [7:0] data_out3_estr;
wire [7:0] data_out4_estr;
wire [7:0] data_out5_estr;
wire [7:0] data_out6_estr;
wire [7:0] data_out7_estr;



   Recirculacion re( /*AUTOINST*/
		    // Outputs
		    .data_out0		(data_out0[7:0]),
		    .data_out1		(data_out1[7:0]),
		    .data_out2		(data_out2[7:0]),
		    .data_out3		(data_out3[7:0]),
		    .data_out4		(data_out4[7:0]),
		    .data_out5		(data_out5[7:0]),
		    .data_out6		(data_out6[7:0]),
		    .data_out7		(data_out7[7:0]),
		    // Inputs
		    .clk_f		(clk_f),
		    .selectorRe		(selectorRe),
		    .data_in0		(data_in0[7:0]),
		    .data_in1		(data_in1[7:0]),
		    .data_in2		(data_in2[7:0]),
		    .data_in3		(data_in3[7:0]),
		    .valid0		(valid0),
		    .valid1		(valid1),
		    .valid2		(valid2),
		    .valid3		(valid3));
    synth_recirculacion re_estr(  /*AUTOINST*/
				// Outputs
				.data_out0	(data_out0[7:0]),
				.data_out1	(data_out1[7:0]),
				.data_out2	(data_out2[7:0]),
				.data_out3	(data_out3[7:0]),
				.data_out4	(data_out4[7:0]),
				.data_out5	(data_out5[7:0]),
				.data_out6	(data_out6[7:0]),
				.data_out7	(data_out7[7:0]),
				// Inputs
				.clk_f		(clk_f),
				.data_in0	(data_in0[7:0]),
				.data_in1	(data_in1[7:0]),
				.data_in2	(data_in2[7:0]),
				.data_in3	(data_in3[7:0]),
				.selectorRe	(selectorRe),
				.valid0		(valid0),
				.valid1		(valid1),
				.valid2		(valid2),
				.valid3		(valid3));
    
   probadorrecirc probador_(  /*AUTOINST*/
			    // Outputs
			    .clk_f		(clk_f),
			    .selectorRe		(selectorRe),
			    .data_in0		(data_in0[7:0]),
			    .data_in1		(data_in1[7:0]),
			    .data_in2		(data_in2[7:0]),
			    .data_in3		(data_in3[7:0]),
			    .valid0		(valid0),
			    .valid1		(valid1),
			    .valid2		(valid2),
			    .valid3		(valid3),
			    .clk_f_estr		(clk_f_estr),
			    .selectorRe_estr	(selectorRe_estr),
			    .data_in0_estr	(data_in0_estr[7:0]),
			    .data_in1_estr	(data_in1_estr[7:0]),
			    .data_in2_estr	(data_in2_estr[7:0]),
			    .data_in3_estr	(data_in3_estr[7:0]),
			    .valid0_estr	(valid0_estr),
			    .valid1_estr	(valid1_estr),
			    .valid2_estr	(valid2_estr),
			    .valid3_estr	(valid3_estr),
			    // Inputs
			    .data_out0		(data_out0[7:0]),
			    .data_out1		(data_out1[7:0]),
			    .data_out2		(data_out2[7:0]),
			    .data_out3		(data_out3[7:0]),
			    .data_out4		(data_out4[7:0]),
			    .data_out5		(data_out5[7:0]),
			    .data_out6		(data_out6[7:0]),
			    .data_out7		(data_out7[7:0]),
			    .data_out0_estr	(data_out0_estr[7:0]),
			    .data_out1_estr	(data_out1_estr[7:0]),
			    .data_out2_estr	(data_out2_estr[7:0]),
			    .data_out3_estr	(data_out3_estr[7:0]),
			    .data_out4_estr	(data_out4_estr[7:0]),
			    .data_out5_estr	(data_out5_estr[7:0]),
			    .data_out6_estr	(data_out6_estr[7:0]),
			    .data_out7_estr	(data_out7_estr[7:0]));
endmodule
